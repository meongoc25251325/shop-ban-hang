const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const sassMiddleware = require('node-sass-middleware');
const breadcrumbs = require('express-breadcrumbs');
const session = require('express-session');

const roleConfigs =  require("./configs/role");

const db = require("./models/index");
db.sequelize.sync();
/*db.sequelize.sync({ force: true }).then(() => {
  console.log("Drop and re-sync db.");
});*/

const indexRouter = require('./routes/index');
const adminRouter = require('./routes/admin');

const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(sassMiddleware({
  src: path.join(__dirname, 'public'),
  dest: path.join(__dirname, 'public'),
  indentedSyntax: true, // true = .sass and false = .scss
  sourceMap: true
}));
app.use(express.static(path.join(__dirname, 'public')));

//
const bodyParser= require('body-parser')
app.use(bodyParser.urlencoded({extended: true}))

//Session
app.use(session({
  resave: false, // don't save session if unmodified
  saveUninitialized: true, // don't create session until something stored
  secret: 'keyboard cat'
}));

app.use(function(req, res, next){
  const err = req.session.error;
  const msg = req.session.success;
  delete req.session.error;
  delete req.session.success;
  res.locals.message = '';
  if (err) res.locals.message = err;
  if (msg) res.locals.message = msg;
  next();
});

//Check user login with admin view
app.use('/admin', function (req, res, next) {
  res.locals.roleModules = roleConfigs.role_mdules;
  if (req.session.user) {
    if(req.session.user.is_admin){
      next();
    }else{
      res.redirect('/');
    }

  } else {
    //next();
    res.redirect('/login');
  }
});

app.use('/', async function (req, res, next) {
  //global session cart on view
  res.locals.sessionCart = req.session.cart || [];
  res.locals.userLogged = req.session.user || null;
  //GET menu categories
  const ProductCategory = db.ProductCategory;
  res.locals.menuCategories = await ProductCategory.findAll({
    order: [
      ['name', 'ASC']
    ],
  });
  next();
});

//Breadcrumbs
app.use(breadcrumbs.init());
app.use(breadcrumbs.setHome());
app.use('/admin', breadcrumbs.setHome({
  name: 'Admin',
  url: '/admin'
}));

//
app.use(function(req, res, next) {
  res.locals.current_url = req.url;
  res.locals.queryInUrl = req.query;
  res.locals.bodyParams = req.body;
  next();
});

//
app.use('/tinymce', express.static(path.join(__dirname, 'node_modules', 'tinymce')));

//Admin router
app.use('/admin', adminRouter);
app.use('/', indexRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
