const db = require("../../models");
const Role = db.Role;
const Op = db.Sequelize.Op;

exports.index = async (req, res, next) => {
    //Check role
    if(!req.session.user.role || !req.session.user.role.actions || !req.session.user.role.actions.role){
        res.redirect("/admin")
    }

    //Setup Page
    req.breadcrumbs({
        name: 'Phân quyền ',
        url: ''
    });
    res.locals.breadcrumbs = req.breadcrumbs();
    res.locals.title = "Quản lý phân quyền";

    //Get data in database
    let where = {};
    if(req.query.name){
        where.title = { [Op.iLike]: `%${req.query.name}%` }
    }
    const page = req.query.page || 1;
    await Role.paginate({
        page: page,
        paginate: 20,
        where: where
    })
        .then(data => {
            const { docs, pages, total } = data;
            //res.send(data);
            let pageLinks = [];
            for(let i = 1; i <= pages ; i++){
                const params = new URLSearchParams(Object.assign({},req.query,{page: i}));
                pageLinks.push('/admin/blogs' + "?" + params.toString());
            }
            res.render('admin/roles/list',{items : docs,pages:pages,pageLinks:pageLinks,total:total});
        })
        .catch(err => {
            res.locals.error = "Không tải được danh sách phân quyền";
            res.render('admin/roles/list',{users : []});
        });
};

exports.detail = async (req, res, next) => {
    //Check role
    if(!req.session.user.role || !req.session.user.role.actions || !req.session.user.role.actions.role){
        res.redirect("/admin")
    }

    //Get user id
    const id = parseInt(req.params.id);
    const title = id ? "Chỉnh sửa phân quyền" : "Thêm phân quyền mới"

    //Set up Page
    req.breadcrumbs([
        {
            name: 'Quản lý phân quyền',
            url: '/admin/roles'
        },
        {
            name: id ? "Chỉnh sửa" : "Tạo mới",
            url: null
        }
    ]);
    res.locals.breadcrumbs = req.breadcrumbs();
    res.locals.title = title;

    //Get user
    if(id){
        Role.findByPk(id)
            .then(data => {
                res.render('admin/roles/detail',{id : id,item : data});
            })
            .catch(err => {
                res.locals.error = `Không tải được phân quyền`;
                res.render('admin/roles/detail',{id : id});
            });
    }else{
        res.render('admin/roles/detail',{id : id});
    }
};

exports.save = async (req, res, next) => {
    //Check role
    if(!req.session.user.role || !req.session.user.role.actions || !req.session.user.role.actions.role){
        res.redirect("/admin")
    }

    //Get user id
    const id = parseInt(req.params.id);
    const title = id ? "Chỉnh sửa phân quyền" : "Thêm phân quyền mới"

    //Set up Page
    req.breadcrumbs([
        {
            name: 'Quản lý phân quyền',
            url: '/admin/roles'
        },
        {
            name: id ? "Chỉnh sửa" : "Tạo mới",
            url: null
        }
    ]);
    res.locals.breadcrumbs = req.breadcrumbs();
    res.locals.title = title;

    if (!req.body.name) {
        res.locals.error = "Tên phân quyền không được bỏ trống";
        return res.render('admin/roles/detail',{id : id});
    }
    let data = {
        name: req.body.name,
        actions: req.body.actions
    };

    if(!id){
        Role.create(data)
            .then(data => {
                res.redirect('/admin/roles/' + data.id);
            })
            .catch(err => {
                res.locals.error = "Có lỗi xảy ra không thể tạo mới";
                res.render('admin/roles/detail',{id : id});
            });
    }else{
        Role.update(data, {
            where: { id: id }
        })
            .then(num => {
                if (num == 1) {
                    res.locals.success_message = "Đã cập nhật thành công";
                    res.redirect('/admin/roles/' + id);
                } else {
                    res.locals.error = `Không thể cập nhật phân quyền với id=${id}`;
                    res.render('admin/roles/detail',{id : id});
                }
            })
            .catch(err => {
                res.locals.error = `Có lỗi xảy ra. Không thể cập nhật phân quyền`;
                res.render('admin/roles/detail',{id : id});
            });
    }
};

exports.delete = async (req, res, next) => {
    //Check role
    if(!req.session.user.role || !req.session.user.role.actions || !req.session.user.role.actions.role){
        res.redirect("/admin")
    }

    const id = req.query.id;
    const ids = req.query.ids;

    if(id){
        Role.destroy({
            where: {
                id: id
            }
        })
            .then(num => {
                res.redirect('/admin/roles');
            })
            .catch(err => {
                res.redirect('/admin/roles');
            });
    }
    if(ids){
        Role.destroy({
            where: {
                id: {
                    [Op.in]: ids,
                }
            }
        })
            .then(num => {
                res.redirect('/admin/roles');
            })
            .catch(err => {
                res.redirect('/admin/roles');
            });
    }


};