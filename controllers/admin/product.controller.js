const db = require("../../models");
const Product = db.Product;
const ProductCategory = db.ProductCategory;
const ProductCategoryMap = db.ProductCategoryMap;
const ProductImages = db.ProductImages;
const ProductAttribute = db.ProductAttribute;
const ProductAttributeItem = db.ProductAttributeItem;
const Attributes = db.Attributes;
const Op = db.Sequelize.Op;

function updatePropertyCategory(product_id,categories){
    if(product_id){
        ProductCategoryMap.destroy({
            where: {
                product_id: product_id
            }
        });
        if(categories){
            for (const category_id of categories) {
                let data_category = {
                    product_id: product_id,
                    product_category_id: category_id,
                }
                ProductCategoryMap.create(data_category);
            }
        }
    }
}
function updatePropertyImages(product_id,images){
    if(product_id){
        ProductImages.destroy({
            where: {
                product_id: product_id
            }
        });
        if(images){
            for (const image of images) {
                let data = {
                    product_id: product_id,
                    image: image,
                }
                ProductImages.create(data);
            }
        }
    }
}
async function updatePropertyAttribute(product_id,attributes){
    if(product_id){
        if(attributes){
            let dont_delete_ids = [];
            for (const attribute of attributes) {
                let id = parseInt( attribute.id) || 0;
                if(id){
                    //Delete Current
                    ProductAttributeItem.destroy({
                        where: {
                            product_attribute_id: id,
                        }
                    });

                }else{
                    //Create
                    let dataProductAttribute = {
                        product_id: product_id,
                        attribute_id: parseInt( attribute.attribute_id),
                    };
                    await ProductAttribute.create(dataProductAttribute)
                        .then((entry) => {
                            id = entry.id
                        })
                }
                dont_delete_ids.push(id);
                //Re-Create product attribute items
                let values = attribute.values;
                for (const value of values) {
                    let data = {
                        product_attribute_id: id,
                        attribute_item_id: parseInt(value),
                    }
                    ProductAttributeItem.create(data);
                }
            }
            ProductAttribute.destroy({
                where: {
                    id: {
                        [Op.notIn]: dont_delete_ids
                    },
                    product_id:product_id
                }
            });
        }
    }
}

exports.index = async (req, res, next) => {
    //Check role
    if(!req.session.user.role || !req.session.user.role.actions || !req.session.user.role.actions.product){
        res.redirect("/admin")
    }

    //Setup Page
    req.breadcrumbs({
        name: 'Sản phẩm',
        url: ''
    });
    res.locals.breadcrumbs = req.breadcrumbs();
    res.locals.title = "Quản lý sản phẩm";

    //Get data in database
    let where = {};
    if(req.query.name){
        where.name = { [Op.iLike]: `%${req.query.name}%` }
    }
    if(req.query.sku){
        where.name = { [Op.iLike]: `%${req.query.sku}%` }
    }
    if(req.query.status){
        where.status = req.query.status
    }
    const page = req.query.page || 1;
    await Product.paginate({
        page: page,
        paginate: 20,
        where: where,
        order: [
            ['id', 'DESC']
        ],
    })
        .then(data => {
            const { docs, pages, total } = data;
            let pageLinks = [];
            for(let i = 1; i <= pages ; i++){
                const params = new URLSearchParams(Object.assign({},req.query,{page: i}));
                pageLinks.push('/admin/product-category' + "?" + params.toString());
            }
            res.render('admin/products/list',{items : docs,pages:pages,pageLinks:pageLinks,total:total});
        })
        .catch(err => {
            res.locals.error = "Không tải được danh sách sản phẩm";
            res.render('admin/products/list',{users : []});
        });
};

exports.delete = async (req, res, next) => {
    //Check role
    if(!req.session.user.role || !req.session.user.role.actions || !req.session.user.role.actions.product){
        res.redirect("/admin")
    }

    const id = req.query.id;
    const ids = req.query.ids;

    if(id){
        Product.destroy({
            where: {
                id: id
            }
        })
            .then(num => {
                res.redirect('/admin/products');
            })
            .catch(err => {
                res.redirect('/admin/products');
            });
    }
    if(ids){
        Product.destroy({
            where: {
                id: {
                    [Op.in]: ids,
                }
            }
        })
            .then(num => {
                res.redirect('/admin/products');
            })
            .catch(err => {
                res.redirect('/admin/products');
            });
    }


};

exports.detail = async (req, res, next) => {
    //Check role
    if(!req.session.user.role || !req.session.user.role.actions || !req.session.user.role.actions.product){
        res.redirect("/admin")
    }

    //Get user id
    const id = parseInt(req.params.id);
    const title = id ? "Chỉnh sửa sản phẩm" : "Thêm sản phẩm mới"

    //Set up Page
    req.breadcrumbs([
        {
            name: 'Quản lý sản phẩm',
            url: '/admin/products'
        },
        {
            name: id ? "Chỉnh sửa" : "Tạo mới",
            url: null
        }
    ]);
    res.locals.breadcrumbs = req.breadcrumbs();
    res.locals.title = title;

    //Get Product category
    let categories = await ProductCategory.findAll();
    let product_attributes = await Attributes.findAll({
        include: [
            {
                model: db.AttributeItems,
                as: 'items'
            }
        ]
    });
    res.locals.product_attributes = product_attributes;
    res.locals.categories = categories;
    res.locals.disable_attribute_ids = [];
    //Get user
    if(id){
        Product.findByPk(id,{
            include: [
                {
                    model: db.ProductCategoryMap,
                    as: 'categories'
                },
                {
                    model: db.ProductImages,
                    as: 'gallery'
                },
                {
                    model: db.ProductAttribute,
                    as: 'attributes',
                    include: [
                        {
                            model: db.Attributes,
                            as: 'attribute',
                        },
                        {
                            model: db.AttributeItems,
                            as: 'items'
                        },
                        {
                            model: db.AttributeItems,
                            as: 'values',
                        }
                    ]
                },
            ]
        })
            .then(async data => {
                data = data.toJSON();
                if(data.attributes){
                    for (let i = 0; i <  data.attributes.length; i++){
                        res.locals.disable_attribute_ids.push(parseInt(data.attributes[i].attribute_id))
                        let value_selected = data.attributes[i].values.map(function(value,index) {
                            return value["id"];
                        })
                        data.attributes[i].attribute_item_ids = value_selected;
                    }

                }
                //res.send(data)
                let category_ids = await data.categories.map(a => parseInt(a.product_category_id)) || [];
                res.render('admin/products/detail',{id : id,item : data,category_ids:category_ids});
            })
            .catch(err => {
                console.log(err);
                res.locals.error = `Không load sản phẩm`;
                res.render('admin/products/detail',{id : id,categories:categories});
            });
    }else{
        res.render('admin/products/detail',{id : id});
    }
};

exports.save = async (req, res, next) => {
    //Check role
    if(!req.session.user.role || !req.session.user.role.actions || !req.session.user.role.actions.product){
        res.redirect("/admin")
    }

    //Get user id
    const id = parseInt(req.params.id);
    const title = id ? "Chỉnh sửa bài viết" : "Thêm bài viết mới"

    //Set up Page
    req.breadcrumbs([
        {
            name: 'Quản lý sản phẩm',
            url: '/admin/products'
        },
        {
            name: id ? "Chỉnh sửa" : "Tạo mới",
            url: null
        }
    ]);
    res.locals.breadcrumbs = req.breadcrumbs();
    res.locals.title = title;

    //Get Product category
    let categories = await ProductCategory.findAll();

    if (!req.body.name) {
        res.locals.error = "Tên sản phẩm không được bỏ trống";
        return res.render('admin/products/detail',{id : id,categories:categories});
    }
    if (!req.body.slug) {
        res.locals.error = "Đường dẫn không được bỏ trống";
        return res.render('admin/products/detail',{id : id,categories:categories});
    }
    if (!req.body.sku) {
        res.locals.error = "Mã sản phẩm không được bỏ trống";
        return res.render('admin/products/detail',{id : id,categories:categories});
    }

    let price = req.body.price || '0';
    let sale_price = req.body.sale_price || '0';

    let data = {
        name: req.body.name,
        slug: req.body.slug,
        sku: req.body.sku,
        description: req.body.description,
        price: parseFloat(price.replace(/,/g, "")),
        sale_price: parseFloat(sale_price.replace(/,/g, "")),
        label: req.body.label,
        image: req.body.image,
        status: req.body.status,
        stock: parseInt(req.body.stock) || 0,
    };

    let category_ids = req.body.category_ids || [];
    category_ids = category_ids.map(a => parseInt(a));

    let product_attributes = req.body.attributes || []

    let gallery = req.body.gallery || [];

    if(!id){
        const exist = await Product.findOne({ where: { slug: data.slug } });
        if (exist === null) {
            Product.create(data)
                .then(data => {
                    updatePropertyCategory(data.id,category_ids);
                    updatePropertyImages(data.id,gallery);
                    updatePropertyAttribute(data.id,product_attributes);
                    res.redirect('/admin/products/' + data.id);
                })
                .catch(err => {
                    console.log(err)
                    res.send(err);
                    res.locals.error = "Có lỗi xảy ra không thể tạo mới";
                    res.render('admin/products/detail',{id : id,categories:categories});
                });
        } else {
            res.locals.error = "Đường dẫn đã tồn tại";
            res.render('admin/products/detail',{id : id,categories:categories});
        }
    }else{
        const exist = await Product.findOne({ where: { slug: data.slug , id : {[Op.ne] : id}} });
        if (exist === null) {
            Product.update(data, {
                where: { id: id }
            })
                .then(num => {
                    if (num == 1) {
                        updatePropertyCategory(id,category_ids);
                        updatePropertyImages(id,gallery);
                        updatePropertyAttribute(id,product_attributes);
                        res.locals.success_message = "Đã cập nhật thành công";
                        res.redirect('/admin/products/' + id);
                    } else {
                        res.locals.error = `Không thể cập nhật sản phẩm với id=${id}`;
                        res.render('admin/products/detail',{id : id,categories:categories});
                    }
                })
                .catch(err => {
                    console.log(err);
                    return res.send(err)
                    res.locals.error = `Có lỗi xảy ra. Không thể cập nhật sản phẩm`;
                    res.render('admin/products/detail',{id : id,categories:categories});
                });
        } else {
            res.locals.error = "Đường dẫn đã tồn tại";
            res.render('admin/products/detail',{id : id,categories:categories});
        }
    }
};

