const db = require("../../models");
const Attributes = db.Attributes;
const AttributeItems = db.AttributeItems;
const Op = db.Sequelize.Op;

exports.index = async (req, res, next) => {
    //Check role
    if(!req.session.user.role || !req.session.user.role.actions || !req.session.user.role.actions.attribute){
        res.redirect("/admin")
    }
    //Setup Page
    req.breadcrumbs({
        name: 'Danh mục thuộc tính',
        url: ''
    });
    res.locals.breadcrumbs = req.breadcrumbs();
    res.locals.title = "Quản lý thuộc tính";

    //Get data in database
    let where = {};
    if(req.query.name){
        where.name = { [Op.iLike]: `%${req.query.name}%` }
    }
    const page = req.query.page || 1;
    await Attributes.paginate({
        page: page,
        paginate: 20,
        where: where
    })
        .then(data => {
            const { docs, pages, total } = data;
            //res.send(data);
            let pageLinks = [];
            for(let i = 1; i <= pages ; i++){
                const params = new URLSearchParams(Object.assign({},req.query,{page: i}));
                pageLinks.push('/admin/attributes' + "?" + params.toString());
            }
            res.render('admin/attributes/list',{items : docs,pages:pages,pageLinks:pageLinks,total:total});
        })
        .catch(err => {
            res.locals.error = "Không tải được danh sách thuộc tính";
            res.render('admin/attributes/list',{items : []});
        });
};

exports.detail = async (req, res, next) => {
    //Check role
    if(!req.session.user.role || !req.session.user.role.actions || !req.session.user.role.actions.attribute){
        res.redirect("/admin")
    }

    //Get id
    const id = parseInt(req.params.id);
    const title = id ? "Chỉnh sửa thuộc tính" : "Thêm thuộc tính mới"

    //Set up Page
    req.breadcrumbs([
        {
            name: 'Quản lý thuộc tính',
            url: '/admin/attributes'
        },
        {
            name: id ? "Chỉnh sửa" : "Tạo mới",
            url: null
        }
    ]);
    res.locals.breadcrumbs = req.breadcrumbs();
    res.locals.title = title;

    //Get data
    if(id){
        Attributes.findByPk(id,{
            include: [
                {
                    model: db.AttributeItems,
                    as: 'items'
                }
            ]
        })
            .then(data => {
                res.render('admin/attributes/detail',{id : id,item : data});
            })
            .catch(err => {
                res.locals.error = `Không load được thông tin`;
                res.render('admin/attributes/detail',{id : id});
            });
    }else{
        res.render('admin/attributes/detail',{id : id});
    }
};

exports.save = async (req, res, next) => {
    //Check role
    if(!req.session.user.role || !req.session.user.role.actions || !req.session.user.role.actions.attribute){
        res.redirect("/admin")
    }
    //Get user id
    const id = parseInt(req.params.id);
    const title = id ? "Chỉnh sửa thuộc tính" : "Thêm thuộc tính mới"

    //Set up Page
    req.breadcrumbs([
        {
            name: 'Quản lý thuộc tính',
            url: '/admin/attribute'
        },
        {
            name: id ? "Chỉnh sửa" : "Tạo mới",
            url: null
        }
    ]);
    res.locals.breadcrumbs = req.breadcrumbs();
    res.locals.title = title;
    if (!req.body.name) {
        res.locals.error = "Tên không được bỏ trống";
        res.render('admin/attribute/detail',{id : id});
    }

    let data = {
        name: req.body.name
    };
    if(!id){
        Attributes.create(data)
            .then(data => {
                res.redirect('/admin/attributes/' + data.id);
            })
            .catch(err => {
                res.locals.error = "Có lỗi xảy ra không thể tạo mới";
                res.render('admin/attributes/detail',{id : id});
            });
    }else{
        Attributes.update(data, {
            where: { id: id }
        })
            .then(num => {
                if (num == 1) {
                    res.locals.success_message = "Đã cập nhật thành công";
                    res.redirect('/admin/attributes/' + id);
                } else {
                    res.locals.error = `Không thể cập nhật danh mục với id=${id}`;
                    res.render('admin/attributes/detail',{id : id});
                }
            })
            .catch(err => {
                res.locals.error = `Có lỗi xảy ra. Không thể cập nhật thuộc tính`;
                res.render('admin/attributes/detail',{id : id});
            });
    }
};

exports.delete = async (req, res, next) => {
    //Check role
    if(!req.session.user.role || !req.session.user.role.actions || !req.session.user.role.actions.attribute){
        res.redirect("/admin")
    }
    //Get user id
    const id = req.query.id;
    const ids = req.query.ids;

    if(id){
        Attributes.destroy({
            where: {
                id: id
            }
        })
            .then(num => {
                res.redirect('/admin/attributes');
            })
            .catch(err => {
                res.redirect('/admin/attributes');
            });
    }
    if(ids){
        Attributes.destroy({
            where: {
                id: {
                    [Op.in]: ids,
                }
            }
        })
            .then(num => {
                res.redirect('/admin/attributes');
            })
            .catch(err => {
                res.redirect('/admin/attributes');
            });
    }


};

exports.addValue = async (req, res, next) => {
    //Check role
    if(!req.session.user.role || !req.session.user.role.actions || !req.session.user.role.actions.attribute){
        res.redirect("/admin")
    }
    if (!req.body.name) {
        res.status(400).send("Giá trị không được bỏ trống")
    }

    let data = {
        attribute_id: req.params.id,
        name: req.body.name,
        background: req.body.background,
    };

    AttributeItems.create(data)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send("Đã xảy ra lỗi khi thêm giá trị")
        });
};

exports.deleteValue = async (req, res, next) => {
    //Check role
    if(!req.session.user.role || !req.session.user.role.actions || !req.session.user.role.actions.attribute){
        res.redirect("/admin")
    }
    let id = parseInt(req.params.id);
    AttributeItems.destroy({
        where: {
            id: id
        }
    })
        .then(num => {
            res.send("Đã xóa thành công");
        })
        .catch(err => {
            res.status(500).send("Đã xảy ra lỗi khi xóa giá trị")
        });
};
