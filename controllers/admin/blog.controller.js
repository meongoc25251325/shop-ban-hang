const db = require("../../models");
const Blog = db.Blog;
const Op = db.Sequelize.Op;

exports.index = async (req, res, next) => {
    //Check role
    if(!req.session.user.role || !req.session.user.role.actions || !req.session.user.role.actions.blog){
        res.redirect("/admin")
    }
    //Setup Page
    req.breadcrumbs({
        name: 'Bài viết',
        url: ''
    });
    res.locals.breadcrumbs = req.breadcrumbs();
    res.locals.title = "Quản lý bài viết";

    //Get data in database
    let where = {};
    if(req.query.title){
        where.title = { [Op.iLike]: `%${req.query.title}%` }
    }
    if(req.query.status){
        where.status = req.query.title
    }
    const page = req.query.page || 1;
    await Blog.paginate({
        page: page,
        paginate: 20,
        where: where
    })
        .then(data => {
            const { docs, pages, total } = data;
            //res.send(data);
            let pageLinks = [];
            for(let i = 1; i <= pages ; i++){
                const params = new URLSearchParams(Object.assign({},req.query,{page: i}));
                pageLinks.push('/admin/blogs' + "?" + params.toString());
            }
            res.render('admin/blogs/list',{items : docs,pages:pages,pageLinks:pageLinks,total:total});
        })
        .catch(err => {
            res.locals.error = "Không tải được danh sách người dùng";
            res.render('admin/blogs/list',{users : []});
        });
};

exports.detail = async (req, res, next) => {
    //Check role
    if(!req.session.user.role || !req.session.user.role.actions || !req.session.user.role.actions.blog){
        res.redirect("/admin")
    }

    //Get user id
    const id = parseInt(req.params.id);
    const title = id ? "Chỉnh sửa bài viết" : "Thêm bài viết mới"

    //Set up Page
    req.breadcrumbs([
        {
            name: 'Quản lý bài viết',
            url: '/admin/blogs'
        },
        {
            name: id ? "Chỉnh sửa" : "Tạo mới",
            url: null
        }
    ]);
    res.locals.breadcrumbs = req.breadcrumbs();
    res.locals.title = title;

    //Get user
    if(id){
        Blog.findByPk(id)
            .then(data => {
                res.render('admin/blogs/detail',{id : id,item : data});
            })
            .catch(err => {
                res.locals.error = `Không load bài viết`;
                res.render('admin/blogs/detail',{id : id});
            });
    }else{
        res.render('admin/blogs/detail',{id : id});
    }
};

exports.save = async (req, res, next) => {
    //Check role
    if(!req.session.user.role || !req.session.user.role.actions || !req.session.user.role.actions.blog){
        res.redirect("/admin")
    }

    //Get user id
    const id = parseInt(req.params.id);
    const title = id ? "Chỉnh sửa bài viết" : "Thêm bài viết mới"

    //Set up Page
    req.breadcrumbs([
        {
            name: 'Quản lý bài viết',
            url: '/admin/blogs'
        },
        {
            name: id ? "Chỉnh sửa" : "Tạo mới",
            url: null
        }
    ]);
    res.locals.breadcrumbs = req.breadcrumbs();
    res.locals.title = title;

    if (!req.body.title) {
        res.locals.error = "Tiêu đề bài viết không được bỏ trống";
        return res.render('admin/blogs/detail',{id : id});
    }
    if (!req.body.slug) {
        res.locals.error = "Đường dẫn không được bỏ trống";
        return res.render('admin/blogs/detail',{id : id});
    }
    let data = {
        title: req.body.title,
        slug: req.body.slug,
        content: req.body.content,
        description: req.body.description,
        image: req.body.image,
        status: req.body.status,
    };
    if(!id){
        const exist = await Blog.findOne({ where: { slug: data.slug } });
        if (exist === null) {
            Blog.create(data)
                .then(data => {
                    res.redirect('/admin/blogs/' + data.id);
                })
                .catch(err => {
                    res.locals.error = "Có lỗi xảy ra không thể tạo mới";
                    res.render('admin/blogs/detail',{id : id});
                });
        } else {
            res.locals.error = "Đường dẫn đã tồn tại";
            res.render('admin/blogs/detail',{id : id});
        }
    }else{
        const exist = await Blog.findOne({ where: { slug: data.slug , id : {[Op.ne] : id}} });
        if (exist === null) {
            Blog.update(data, {
                where: { id: id }
            })
                .then(num => {
                    if (num == 1) {
                        res.locals.success_message = "Đã cập nhật thành công";
                        res.redirect('/admin/blogs/' + id);
                    } else {
                        res.locals.error = `Không thể cập nhật bài viết với id=${id}`;
                        res.render('admin/blogs/detail',{id : id});
                    }
                })
                .catch(err => {
                    res.locals.error = `Có lỗi xảy ra. Không thể cập nhật bài viết`;
                    res.render('admin/blogs/detail',{id : id});
                });
        } else {
            res.locals.error = "Đường dẫn đã tồn tại";
            res.render('admin/blogs/detail',{id : id});
        }
    }
};

exports.delete = async (req, res, next) => {
    //Check role
    if(!req.session.user.role || !req.session.user.role.actions || !req.session.user.role.actions.blog){
        res.redirect("/admin")
    }
    const id = req.query.id;
    const ids = req.query.ids;

    if(id){
        Blog.destroy({
            where: {
                id: id
            }
        })
            .then(num => {
                res.redirect('/admin/blogs');
            })
            .catch(err => {
                res.redirect('/admin/blogs');
            });
    }
    if(ids){
        Blog.destroy({
            where: {
                id: {
                    [Op.in]: ids,
                }
            }
        })
            .then(num => {
                res.redirect('/admin/blogs');
            })
            .catch(err => {
                res.redirect('/admin/blogs');
            });
    }


};