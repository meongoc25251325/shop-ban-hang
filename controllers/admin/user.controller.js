const bcrypt = require("bcrypt");
const db = require("../../models");
const User = db.user;
const Role = db.Role;
const Op = db.Sequelize.Op;

//Listing user
exports.index = async (req, res, next) => {
    //Check role
    if(!req.session.user.role || !req.session.user.role.actions || !req.session.user.role.actions.user){
        res.redirect("/admin")
    }

    //Setup Page
    req.breadcrumbs({
        name: 'Quản lý người dùng',
        url: ''
    });
    res.locals.breadcrumbs = req.breadcrumbs();
    res.locals.title = "Quản lý người dùng";

    //Build filter
    let where = {};
    if(req.query.username){
        where.username = { [Op.iLike]: `%${req.query.username}%` }
    }
    if(req.query.phone_number){
        where.phone_number = { [Op.iLike]: `%${req.query.phone_number}%` }
    }
    if(req.query.address){
        where.address = { [Op.iLike]: `%${req.query.address}%` }
    }
    if(req.query.full_name){
        where.full_name = { [Op.iLike]: `%${req.query.full_name}%` }
    }
    if(req.query.status){
        where.status = req.query.status
    }
    const page = req.query.page || 1;

    //Get Users
    await User.paginate({
        page: page,
        paginate: 20,
        where: where
    })
        .then(data => {
            const { docs, pages, total } = data;
            //res.send(data);
            let pageLinks = [];
            for(let i = 1; i <= pages ; i++){
                const params = new URLSearchParams(Object.assign({},req.query,{page: i}));
                pageLinks.push('/admin/users' + "?" + params.toString());
            }
            res.render('admin/users/list',{users : docs,pages:pages,pageLinks:pageLinks,total:total});
        })
        .catch(err => {
            console.log(err);
            res.locals.error = "Không tải được danh sách người dùng";
            res.render('admin/users/list',{users : []});
        });
};

//Detail
exports.detail = async (req, res, next) => {
    //Check role
    if(!req.session.user.role || !req.session.user.role.actions || !req.session.user.role.actions.user){
        res.redirect("/admin")
    }

    //Get user id
    const user_id = parseInt(req.params.id);
    const title = user_id ? "Chỉnh sửa người dùng" : "Thêm người dùng mới"

    //Set up Page
    req.breadcrumbs([
        {
            name: 'Quản lý người dùng',
            url: '/admin/users'
        },
        {
            name: user_id ? "Chỉnh sửa" : "Tạo mới",
            url: null
        }
    ]);
    res.locals.breadcrumbs = req.breadcrumbs();
    res.locals.title = title;

    //Get roles
    res.locals.roles = await Role.findAll();

    //Get user
    if(user_id){
        User.findByPk(user_id)
            .then(data => {
                //res.send(data);
                res.render('admin/users/detail',{userId : user_id,userDetail : data});
            })
            .catch(err => {
                res.locals.error = `Không load được thông tin người dùng`;
                res.render('admin/users/detail',{userId : user_id});
            });
    }else{
        res.render('admin/users/detail',{userId : user_id});
    }
};

//Save (create or update)
exports.save = async (req, res, next) => {
    //Check role
    if(!req.session.user.role || !req.session.user.role.actions || !req.session.user.role.actions.user){
        res.redirect("/admin")
    }

    //Get user id
    const user_id = parseInt(req.params.id);
    const title = user_id ? "Chỉnh sửa người dùng" : "Thêm người dùng mới"

    //Set up Page
    req.breadcrumbs([
        {
            name: 'Quản lý người dùng',
            url: '/admin/users'
        },
        {
            name: user_id ? "Chỉnh sửa" : "Tạo mới",
            url: null
        }
    ]);
    res.locals.breadcrumbs = req.breadcrumbs();
    res.locals.title = title;

    res.locals.roles = await Role.findAll();

    //Hash password
    let password = null;
    if(req.body.password){
        const salt = await bcrypt.genSalt(10);
        password = await bcrypt.hash(req.body.password, salt);
    }

    if(!user_id){
        //Check validate password
        if (!req.body.username) {
            res.locals.error = "Tên tài khoản không được bỏ trống";
            return res.render('admin/users/detail',{userId : user_id,userDetail:req.body});
        }
        if (!req.body.password) {
            res.locals.error = "Mật khẩu không được bỏ trống";
            return res.render('admin/users/detail',{userId : user_id});
        }
        if (req.body.password !== req.body.repassword) {
            res.locals.error = "Nhập lại mật khẩu không chính xác";
            return res.render('admin/users/detail',{userId : user_id});
        }

        //Create
        let userData = {
            first_name: req.body.first_name,
            last_name: req.body.last_name,
            full_name: req.body.first_name + ' ' + req.body.last_name,
            username: req.body.username,
            password: password,
            phone_number: req.body.phone_number,
            address: req.body.address,
            status: req.body.status,
            is_admin: req.body.is_admin == 'true' ? true : false,
            role_id: parseInt(req.body.role_id),
        };

        const userExist = await User.findOne({ where: { username: userData.username } });
        if (userExist === null) {
            User.create(userData)
                .then(data => {
                    res.redirect('/admin/users/' + data.id);
                })
                .catch(err => {
                    res.locals.error = "Có lỗi xảy ra không thể đăng nhập";
                    res.render('admin/users/detail',{userId : user_id,userDetail : req.body});
                });
        } else {
            res.locals.error = "Tài khoản đã tồn tại";
            res.render('admin/users/detail',{userId : user_id,userDetail : req.body});
        }
    }else{
        if(password){
            //Check validate password
            if (!req.body.password) {
                res.locals.error = "Mật khẩu không được bỏ trống";
                return res.render('admin/users/detail',{userId : user_id,userDetail:req.body});
            }
            if (req.body.password !== req.body.repassword) {
                res.locals.error = "Nhập lại mật khẩu không chính xác";
                return res.render('admin/users/detail',{userId : user_id,userDetail:req.body});
            }
        }
        //Update
        let userData = {
            first_name: req.body.first_name,
            last_name: req.body.last_name,
            full_name: req.body.first_name + ' ' + req.body.last_name,
            username: req.body.username,
            phone_number: req.body.phone_number,
            address: req.body.address,
            status: req.body.status,
            is_admin: req.body.is_admin == 'true' ? true : false,
            role_id: parseInt(req.body.role_id),
        };
        if(password){
            userData.password = password;
        }

        User.update(userData, {
            where: { id: user_id }
        })
            .then(num => {
                if (num == 1) {
                    res.locals.success_message = "Đã cập nhật thành công";
                    res.redirect('/admin/users/' + user_id);
                } else {
                    res.locals.error = `Không thể cập nhật người dùng với id=${user_id}. Có lẽ người dùng không tồn tại`;
                    res.render('admin/users/detail',{userId : user_id,userDetail:req.body});
                }
            })
            .catch(err => {
                res.send(err);
                res.locals.error = `Có lỗi xảy ra. Không thể cập nhật người dùng`;
                res.render('admin/users/detail',{userId : user_id,userDetail:req.body});
            });
    }
};

//Delete users
exports.delete = async (req, res, next) => {
    //Check role
    if(!req.session.user.role || !req.session.user.role.actions || !req.session.user.role.actions.user){
        res.redirect("/admin")
    }

    //Get user id
    const id = req.query.id;
    const ids = req.query.ids;

    if(id){
        User.destroy({
            where: {
                id: id
            }
        })
            .then(num => {
                res.redirect('/admin/users/');
            })
            .catch(err => {
                res.redirect('/admin/users/');
            });
    }
    if(ids){
        User.destroy({
            where: {
                id: {
                    [Op.in]: ids,
                }
            }
        })
            .then(num => {
                res.redirect('/admin/users/');
            })
            .catch(err => {
                res.redirect('/admin/users/');
            });
    }


};

exports.create = async (res, req, next) => {
    //Check role
    if(!req.session.user.role || !req.session.user.role.actions || !req.session.user.role.actions.user){
        res.redirect("/admin")
    }

    //Check required data
    if (!req.body.username) {
        res.locals.error = "Tên tài khoản không được bỏ trống";
    }
    if (!req.body.password) {
        res.locals.error = "Mật khẩu không được bỏ trống";
    }
    if (req.body.password !== req.body.re_password) {
        res.locals.error = "Nhập lại mật khẩu không chính xác";
    }

    //Hash password
    const salt = await bcrypt.genSalt(10);
    const password = await bcrypt.hash(req.body.password, salt);
    // Create a Tutorial
    const userData = {
        username: req.body.username,
        password: password,
        phone_number: req.body.phone_number,
        address: req.body.address,
        status: "Active"
    };

    User.create(userData)
        .then(data => {
            res.redirect('/admin/users/' + data.id);
        })
        .catch(err => {
            res.locals.error = "Có lỗi xảy ra không thể đăng nhập";
            res.redirect('/admin/users/0');
        });
};