const db = require("../../models");
const ProductCategory = db.ProductCategory;
const Op = db.Sequelize.Op;

exports.index = async (req, res, next) => {
    //Check role
    if(!req.session.user.role || !req.session.user.role.actions || !req.session.user.role.actions.product_category){
        res.redirect("/admin")
    }


    //Setup Page
    req.breadcrumbs({
        name: 'Danh mục sản phẩm',
        url: ''
    });
    res.locals.breadcrumbs = req.breadcrumbs();
    res.locals.title = "Quản lý danh mục sản phẩm";

    //Get data in database
    let where = {};
    if(req.query.name){
        where.name = { [Op.iLike]: `%${req.query.name}%` }
    }
    const page = req.query.page || 1;
    await ProductCategory.paginate({
        page: page,
        paginate: 20,
        where: where
    })
        .then(data => {
            const { docs, pages, total } = data;
            //res.send(data);
            let pageLinks = [];
            for(let i = 1; i <= pages ; i++){
                const params = new URLSearchParams(Object.assign({},req.query,{page: i}));
                pageLinks.push('/admin/product-category' + "?" + params.toString());
            }
            res.render('admin/product-categories/list',{categories : docs,pages:pages,pageLinks:pageLinks,total:total});
        })
        .catch(err => {
            res.locals.error = "Không tải được danh sách người dùng";
            res.render('admin/product-categories/list',{users : []});
        });
};

exports.detail = async (req, res, next) => {
    //Check role
    if(!req.session.user.role || !req.session.user.role.actions || !req.session.user.role.actions.product_category){
        res.redirect("/admin")
    }

    //Get user id
    const id = parseInt(req.params.id);
    const title = id ? "Chỉnh sửa danh mục" : "Thêm danh mục mới"

    //Set up Page
    req.breadcrumbs([
        {
            name: 'Quản lý danh mục',
            url: '/admin/product-category'
        },
        {
            name: id ? "Chỉnh sửa" : "Tạo mới",
            url: null
        }
    ]);
    res.locals.breadcrumbs = req.breadcrumbs();
    res.locals.title = title;

    //Get user
    if(id){
        ProductCategory.findByPk(id)
            .then(data => {
                res.render('admin/product-categories/detail',{id : id,detail : data});
            })
            .catch(err => {
                res.locals.error = `Không load được thông tin người dùng`;
                res.render('admin/product-categories/detail',{id : id});
            });
    }else{
        res.render('admin/product-categories/detail',{id : id});
    }
};

exports.save = async (req, res, next) => {
    //Check role
    if(!req.session.user.role || !req.session.user.role.actions || !req.session.user.role.actions.product_category){
        res.redirect("/admin")
    }
    //Get user id
    const id = parseInt(req.params.id);
    const title = id ? "Chỉnh sửa danh mục" : "Thêm danh mục mới"

    //Set up Page
    req.breadcrumbs([
        {
            name: 'Quản lý danh mục',
            url: '/admin/product-category'
        },
        {
            name: id ? "Chỉnh sửa" : "Tạo mới",
            url: null
        }
    ]);
    res.locals.breadcrumbs = req.breadcrumbs();
    res.locals.title = title;
    if (!req.body.name) {
        res.locals.error = "Tên danh mục không được bỏ trống";
        res.render('admin/product-categories/detail',{id : id});
    }
    if (!req.body.slug) {
        res.locals.error = "Đường dẫn không được bỏ trống";
        res.render('admin/product-categories/detail',{id : id});
    }
    let data = {
        name: req.body.name,
        slug: req.body.slug,
        description: req.body.description,
        image: req.body.image,
    };
    if(!id){
        const exist = await ProductCategory.findOne({ where: { slug: data.slug } });
        if (exist === null) {
            ProductCategory.create(data)
                .then(data => {
                    res.redirect('/admin/product-category/' + data.id);
                })
                .catch(err => {
                    res.locals.error = "Có lỗi xảy ra không thể tạo mới";
                    res.render('admin/product-categories/detail',{id : id});
                });
        } else {
            res.locals.error = "Đường dẫn đã tồn tại";
            res.render('admin/product-categories/detail',{id : id});
        }
    }else{
        const exist = await ProductCategory.findOne({ where: { slug: data.slug , id : {[Op.ne] : id}} });
        if (exist === null) {
            ProductCategory.update(data, {
                where: { id: id }
            })
                .then(num => {
                    if (num == 1) {
                        res.locals.success_message = "Đã cập nhật thành công";
                        res.redirect('/admin/product-category/' + id);
                    } else {
                        res.locals.error = `Không thể cập nhật danh mục với id=${id}`;
                        res.render('admin/product-categories/detail',{id : id});
                    }
                })
                .catch(err => {
                    res.locals.error = `Có lỗi xảy ra. Không thể cập nhật danh mục`;
                    res.render('admin/product-categories/detail',{id : id});
                });
        } else {
            res.locals.error = "Đường dẫn đã tồn tại";
            res.render('admin/product-categories/detail',{id : id});
        }
    }
};

exports.delete = async (req, res, next) => {
    //Check role
    if(!req.session.user.role || !req.session.user.role.actions || !req.session.user.role.actions.product_category){
        res.redirect("/admin")
    }
    //Get user id
    const id = req.query.id;
    const ids = req.query.ids;

    if(id){
        ProductCategory.destroy({
            where: {
                id: id
            }
        })
            .then(num => {
                res.redirect('/admin/product-category');
            })
            .catch(err => {
                res.redirect('/admin/product-category');
            });
    }
    if(ids){
        ProductCategory.destroy({
            where: {
                id: {
                    [Op.in]: ids,
                }
            }
        })
            .then(num => {
                res.redirect('/admin/product-category');
            })
            .catch(err => {
                res.redirect('/admin/product-category');
            });
    }


};