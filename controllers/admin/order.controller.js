const db = require("../../models");
const Product = db.Product;
const ProductCategory = db.ProductCategory;
const ProductCategoryMap = db.ProductCategoryMap;
const ProductImages = db.ProductImages;
const ProductAttribute = db.ProductAttribute;
const ProductAttributeItem = db.ProductAttributeItem;
const Order = db.Order;
const OrderItem = db.OrderItem;
const Op = db.Sequelize.Op;


exports.index = async (req, res, next) => {
    //Check role
    if(!req.session.user.role || !req.session.user.role.actions || !req.session.user.role.actions.order){
        res.redirect("/admin")
    }

    //Setup Page
    req.breadcrumbs({
        name: 'Đơn đặt hàng',
        url: ''
    });
    res.locals.breadcrumbs = req.breadcrumbs();
    res.locals.title = "Quản lý đơn hàng";

    //Get data in database
    let where = {};
    let user_where = {};
    if(req.query.order_number){
        where.name = { [Op.iLike]: `%${req.query.order_number}%` }
    }
    if(req.query.status){
        where.status = req.query.status
    }
    if(req.query.phone_number_2){
        where.phone_number = { [Op.iLike]: `%${req.query.phone_number_2}%` }
    }
    if(req.query.phone_number_1){
        user_where.phone_number = { [Op.iLike]: `%${req.query.phone_number_2}%` }
    }
    const page = req.query.page || 1;
    await Order.paginate({
        page: page,
        paginate: 20,
        where: where,
        order: [
            ['id', 'DESC']
        ],
        include:[
            {
                model: db.OrderItem,
                as: 'items'
            },
            {
                model: db.user,
                as: 'user',
                where: user_where
            }
        ],
    })
        .then(data => {
            const { docs, pages, total } = data;
            let pageLinks = [];
            for(let i = 1; i <= pages ; i++){
                const params = new URLSearchParams(Object.assign({},req.query,{page: i}));
                pageLinks.push('/admin/product-category' + "?" + params.toString());
            }
            res.render('admin/orders/list',{items : docs,pages:pages,pageLinks:pageLinks,total:total});
        })
        .catch(err => {
            res.locals.error = "Không tải được danh sách đơn hàng";
            res.render('admin/orders/list',{items : []});
        });
};

exports.delete = async (req, res, next) => {
    //Check role
    if(!req.session.user.role || !req.session.user.role.actions || !req.session.user.role.actions.order){
        res.redirect("/admin")
    }
    const id = req.query.id;
    const ids = req.query.ids;

    if(id){
        Order.destroy({
            where: {
                id: id
            }
        })
            .then(num => {
                OrderItem.destroy({
                    where: {
                        order_id: id
                    }
                });
                res.redirect('/admin/products');
            })
            .catch(err => {
                res.redirect('/admin/products');
            });
    }
    if(ids){
        Order.destroy({
            where: {
                id: {
                    [Op.in]: ids,
                }
            }
        })
            .then(num => {
                OrderItem.destroy({
                    where: {
                        order_id: {
                            [Op.in]: ids,
                        }
                    }
                });
                res.redirect('/admin/products');
            })
            .catch(err => {
                res.redirect('/admin/products');
            });
    }


};

exports.detail = async (req, res, next) => {
    //Check role
    if(!req.session.user.role || !req.session.user.role.actions || !req.session.user.role.actions.order){
        res.redirect("/admin")
    }

    //Get user id
    const id = parseInt(req.params.id);
    const title = "Chỉnh sửa đơn hàng"

    //Set up Page
    req.breadcrumbs([
        {
            name: 'Quản lý đơn hàng',
            url: '/admin/orders'
        },
        {
            name:"Chỉnh sửa",
            url: null
        }
    ]);
    res.locals.breadcrumbs = req.breadcrumbs();
    res.locals.title = title;

    Order.findByPk(id,{
        include:[
            {
                model: db.OrderItem,
                as: 'items'
            },
            {
                model: db.user,
                as: 'user'
            }
        ],
    })
        .then(async data => {
            res.render('admin/orders/detail',{id : id,item : data});
        })
        .catch(err => {
            res.locals.error = `Không tải đơn hàng`;
            res.render('admin/orders/detail',{id : id});
        });
};

exports.save = async (req, res, next) => {
    //Check role
    if(!req.session.user.role || !req.session.user.role.actions || !req.session.user.role.actions.order){
        res.redirect("/admin")
    }

    //Get user id
    const id = parseInt(req.params.id);

    //Set up Page
    req.breadcrumbs([
        {
            name: 'Quản lý sản phẩm',
            url: '/admin/products'
        },
        {
            name: "Chỉnh sửa",
            url: null
        }
    ]);
    res.locals.breadcrumbs = req.breadcrumbs();
    res.locals.title = "Chi tiết đơn hàng";


    let data = {
        status: req.body.status,
    };
    Order.update(data, {
        where: { id: id }
    })
        .then(num => {
            if (num == 1) {
                res.redirect('/admin/orders/' + id);
            } else {
                res.locals.error = `Không thể cập nhật sản phẩm với id=${id}`;
                res.render('admin/products/detail',{id : id,categories:categories});
            }
        })
        .catch(err => {
            res.redirect('/admin/orders/' + id);
        });
};