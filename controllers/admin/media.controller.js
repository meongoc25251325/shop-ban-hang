const db = require("../../models");
const Media = db.media;
const Op = db.Sequelize.Op;

exports.index = async (req, res, next) => {
    //Check role
    if(!req.session.user.role || !req.session.user.role.actions || !req.session.user.role.actions.media){
        res.redirect("/admin")
    }

    //Setup Page
    req.breadcrumbs({
        name: 'Quản lý hình ảnh',
        url: ''
    });
    res.locals.breadcrumbs = req.breadcrumbs();
    res.locals.title = "Quản lý hình ảnh";
    res.locals.dont_load_media_editor = true;

    //Get data in database
    await Media.findAll({
        order: [
            ['id', 'DESC'],
            ['name', 'ASC'],
        ],
    })
        .then(data => {
            res.render('admin/media/list',{medias : data});
        })
        .catch(err => {
            res.locals.error = "Không tải được danh sách người dùng";
            res.render('admin/media/list',{medias : []});
        });
};

exports.json = async (req, res, next) => {
    const page = req.query.page || 1;
    await Media.paginate({
        page: page,
        paginate: 1000,
        order: [
            ['id', 'DESC'],
            ['name', 'ASC'],
        ],
    })
        .then(data => {
            const { docs, pages, total } = data;
            let pageLinks = [];
            res.json(data);
        })
        .catch(err => {
            res.json({})
        });
};

exports.upload = async (req, res, next) => {
    const file = req.file
    if (!file) {
        const error = new Error('Please upload a file')
        error.httpStatusCode = 400
        return next(error)
    }
    const mediaData = {
        name: file.filename,
        url:'/uploads/' +  file.filename
    };

    Media.create(mediaData)
        .then(data => {
            res.json(data)
        })
        .catch(err => {
            res.json({error: "Can't create image"})
        });
};

exports.delete = async (req, res, next) => {
    //Check role
    if(!req.session.user.role || !req.session.user.role.actions || !req.session.user.role.actions.media){
        res.redirect("/admin")
    }
    const id = req.params.id;
    if(id){
        Media.destroy({
            where: {
                id: id
            }
        })
            .then(num => {
                if(num){
                    res.send("Xóa thành công")
                }else{
                    res.send("Không tìm thấy dữ liệu để xóa")
                }
            })
            .catch(err => {
                res.status(400).send("Đã xảy ra lỗi khi xóa ảnh")
            });
    }
};