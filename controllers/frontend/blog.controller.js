const bcrypt = require("bcrypt");
const db = require("../../models");
const Op = db.Sequelize.Op;
const Blog = db.Blog;

exports.blogs = async (req, res, next) => {
    const page = req.query.page || 1;
    await Blog.paginate({
        page: page,
        paginate: 9
    })
        .then(data => {
            res.locals.title = "Tin tức";
            const { docs, pages, total } = data;
            let pageLinks = [];
            for(let i = 1; i <= pages ; i++){
                const params = new URLSearchParams(Object.assign({},req.query,{page: i}));
                pageLinks.push('/tin-tuc' + "?" + params.toString());
            }
            res.render('frontend/blog',{items : docs,pages:pages,pageLinks:pageLinks,total:total});
        })
        .catch(err => {
            res.redirect('/404');
        });
};
exports.blogDetail = async (req, res, next) => {
    let slug = req.params.slug;
    if(!slug) res.redirect('/404');
    let item = await Blog.findOne({ where: { slug: slug } });
    if(!item) res.redirect('/404');
    res.locals.title = item.title;
    res.render('frontend/blog-detail', {item:item });
};




