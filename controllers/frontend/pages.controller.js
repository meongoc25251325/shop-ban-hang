const bcrypt = require("bcrypt");
const db = require("../../models");
const Op = db.Sequelize.Op;
const Blog = db.Blog;
const Product = db.Product


exports.home = async (req, res, next) => {
    res.locals.title = "Eshop chuyên quần áo nữ";
    //GET blogs
    let blogs = [];
    await Blog.paginate({
        page: 1,
        paginate: 3
    })
        .then(data => {
            const { docs, pages, total } = data;
            blogs = docs;
        });

    //GET HOT product
    let hot_product = [];
    await Product.paginate({
        page: 1,
        paginate: 6,
        where: {
            label : 'HOT'
        },
        order: [
            ['id', 'DESC']
        ],
    })
        .then(data => {
            const { docs, pages, total } = data;
            hot_product = docs;
        });


    //GET HOT product
    let new_product = [];
    await Product.paginate({
        page: 1,
        paginate: 6,
        where: {
            label : 'NEW'
        },
        order: [
            ['id', 'DESC']
        ],
    })
        .then(data => {
            const { docs, pages, total } = data;
            new_product = docs;
        });


    //GET HOT product
    let sale_product = [];
    await Product.paginate({
        page: 1,
        paginate: 6,
        where: {
            label : 'SALE'
        },
        order: [
            ['id', 'DESC']
        ],
    })
        .then(data => {
            const { docs, pages, total } = data;
            sale_product = docs;
        });

    res.render('frontend/home', {blogs:blogs,sale_product:sale_product,hot_product:hot_product,new_product:new_product});
};

exports.about = async (req, res, next) => {
    res.render('frontend/about', { title: 'Về chúng tôi' });
};

exports.contact = async (req, res, next) => {
    res.render('frontend/contact', { title: 'Liên hệ' });
};

exports.login = async (req, res, next) => {
    if(req.session.user){
        res.redirect('/tai-khoan');
    }
    res.render('frontend/login', { title: 'Đăng nhập' });
};

exports.register = async (req, res, next) => {
    if(req.session.user){
        res.redirect('/tai-khoan');
    }
    res.render('frontend/register', { title: 'Đăng ký' });
};

exports.page404 = async (req, res, next) => {
    res.render('frontend/404', { title: 'Trang không tồn tại' });
};




