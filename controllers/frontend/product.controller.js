const bcrypt = require("bcrypt");
const moment = require('moment');
const db = require("../../models");
const Op = db.Sequelize.Op;
const Product = db.Product;
const ProductCategory = db.ProductCategory;
const ProductCategoryMap = db.ProductCategoryMap;
const ProductImages = db.ProductImages;
const ProductAttribute = db.ProductAttribute;
const ProductAttributeItem = db.ProductAttributeItem;
const Attributes = db.Attributes;
const Order = db.Order;
const OrderItem = db.OrderItem;

function shallowEqual(object1, object2) {
    const keys1 = Object.keys(object1);
    const keys2 = Object.keys(object2);
    if (keys1.length !== keys2.length) {
        return false;
    }
    for (let key of keys1) {
        if (object1[key] !== object2[key]) {
            return false;
        }
    }
    return true;
}

exports.products = async (req, res, next) => {
    res.locals.title = "Tất cả sản phẩm";
    res.locals.productCategories = await ProductCategory.findAll({
        order: [
            ['name', 'ASC']
        ],
    });
    let products = [];
    let page = req.query.page || 1;
    let where = {
        status: "Publish",
        price : {
            [Op.and] : [

            ]
        }
    };
    if(req.query.s){
        where.name = { [Op.iLike]: `%${req.query.s}%` }
    }
    if(req.query.start_price){
        where.price[Op.and].push({ [Op.gte]: parseInt(req.query.start_price) })
    }
    if(req.query.end_price){
        where.price[Op.and].push({ [Op.lte]: parseInt(req.query.end_price) })
    }
    await Product.paginate({
        page: page,
        paginate: 18,
        where: where,
        order: [
            ['id', 'DESC']
        ],
    })
        .then(data => {
            const { docs, pages, total } = data;
            let pageLinks = [];
            for(let i = 1; i <= pages ; i++){
                const params = new URLSearchParams(Object.assign({},req.query,{page: i}));
                pageLinks.push('/san-pham' + "?" + params.toString());
            }
            products = docs;
            res.locals.pages = pages;
            res.locals.pageLinks = pageLinks;
            res.locals.total = total;

            //res.render('admin/products/list',{items : docs,pages:pages,pageLinks:pageLinks,total:total});
        })
        .catch(err => {
            res.redirect('/404');
        });

    res.render('frontend/product-list', { products:products });
};

exports.productDetail = async (req, res, next) => {
    let slug = req.params.slug;
    if(!slug) res.redirect('/404');
    let item = await Product.findOne({
        include: [
            {
                model: db.ProductCategoryMap,
                as: 'categories'
            },
            {
                model: db.ProductImages,
                as: 'gallery'
            },
            {
                model: db.ProductAttribute,
                as: 'attributes',
                include: [
                    {
                        model: db.Attributes,
                        as: 'attribute',
                    },
                    {
                        model: db.AttributeItems,
                        as: 'items'
                    },
                    {
                        model: db.AttributeItems,
                        as: 'values',
                    }
                ]
            },
        ],
        where: { slug: slug }
    });
    if(!item) res.redirect('/404');
    res.locals.title = item.name;
    res.render('frontend/product-detail', { item:item });
};

exports.productCategory = async (req, res, next) => {
    //GET category
    let slug = req.params.slug;
    if(!slug) res.redirect('/404');
    let item = await ProductCategory.findOne({
        where: { slug: slug }
    });
    if(!item) res.redirect('/404');
    res.locals.title = item.name;

    //GET product in category
    let products = [];
    let page = req.query.page || 1;
    let where = {
        status: "Publish",
    };
    let orderBy = ['name', 'ASC'];
    let orderKey = req.query.orderby || 'name';
    switch (orderKey) {
        case "price_desc":
            orderBy = [['price', 'DESC'],['sale_price', 'DESC']];
            break;
        case "price_asc":
            orderBy = [['price', 'ASC'],['sale_price', 'ASC']];
            break;
        default:
            orderBy = [['name', 'ASC']];
            break;
    }

    await Product.paginate({
        page: page,
        paginate: 18,
        where: where,
        order: orderBy,
        include: [
            {
                model: db.ProductCategoryMap,
                as: 'categories',
                where: {
                    product_category_id : item.id
                }
            },
        ]
    })
        .then(data => {
            const { docs, pages, total } = data;
            let pageLinks = [];
            for(let i = 1; i <= pages ; i++){
                const params = new URLSearchParams(Object.assign({},req.query,{page: i}));
                pageLinks.push('/danh-muc/' + slug + "?" + params.toString());
            }
            products = docs;
            res.locals.pages = pages;
            res.locals.pageLinks = pageLinks;
            res.locals.total = total;

            //res.render('admin/products/list',{items : docs,pages:pages,pageLinks:pageLinks,total:total});
        })
        .catch(err => {
            res.redirect('/404');
        });
    res.render('frontend/product-category', { products:products,item:item  });
};

exports.addToCart = async (req, res, next) => {
    let product_id = parseInt(req.query.product_id);
    let quant = parseInt(req.query.quant);
    let attributes = req.query.attribute || {};

    if(!req.session.cart) req.session.cart = [];
    //Get product detail
    let product = await Product.findByPk(product_id,{
        include: [
            {
                model: db.ProductImages,
                as: 'gallery'
            },
            {
                model: db.ProductAttribute,
                as: 'attributes',
                include: [
                    {
                        model: db.Attributes,
                        as: 'attribute',
                    },
                    {
                        model: db.AttributeItems,
                        as: 'items'
                    },
                    {
                        model: db.AttributeItems,
                        as: 'values',
                    }
                ]
            },
        ]
    });

    let new_item = true;
    for(let i = 0 ; i < req.session.cart.length; i++){
        if(req.session.cart[i].product_id == product_id && shallowEqual(req.session.cart[i].attributes ,attributes)){
            req.session.cart[i].quantity = req.session.cart[i].quantity + quant;
            new_item = false
        }
    }
    if(new_item){
        req.session.cart.push({
            product_id:product_id,
            attributes:attributes,
            quantity:quant,
            image: product.image,
            slug: product.slug,
            name: product.name,
            stock: product.stock || 0,
            price: product.sale_price ? product.sale_price : product.price
        })
    }

    res.redirect("/gio-hang")
};

exports.addToCartAjax = async (req, res, next) => {
    let product_id = parseInt(req.query.product_id);
    let quant = parseInt(req.query.quant);
    let attributes = req.query.attribute || {};

    if(!req.session.cart) req.session.cart = [];
    //Get product detail
    let product = await Product.findByPk(product_id,{
        include: [
            {
                model: db.ProductImages,
                as: 'gallery'
            },
            {
                model: db.ProductAttribute,
                as: 'attributes',
                include: [
                    {
                        model: db.Attributes,
                        as: 'attribute',
                    },
                    {
                        model: db.AttributeItems,
                        as: 'items'
                    },
                    {
                        model: db.AttributeItems,
                        as: 'values',
                    }
                ]
            },
        ]
    });

    let new_item = true;
    for(let i = 0 ; i < req.session.cart.length; i++){
        if(req.session.cart[i].product_id == product_id && shallowEqual(req.session.cart[i].attributes ,attributes)){
            req.session.cart[i].quantity = req.session.cart[i].quantity + quant;
            new_item = false
        }
    }
    if(new_item){
        req.session.cart.push({
            product_id:product_id,
            attributes:attributes,
            quantity:quant,
            image: product.image,
            slug: product.slug,
            name: product.name,
            stock: product.stock || 0,
            price: product.sale_price ? product.sale_price : product.price
        })
    }
    res.render('frontend/_partials/mini-cart',{sessionCart: req.session.cart});
};

exports.removeToCartAjax = async (req, res, next) => {
    let index = parseInt(req.query.index);
    if(req.session.cart){
        req.session.cart.splice(index, 1);
    }
    res.render('frontend/_partials/mini-cart',{sessionCart: req.session.cart});
};

exports.updateCart = async (req, res, next) => {
    let index = parseInt(req.query.index);
    let quantity = parseInt(req.query.quantity);
    let totalCart = 0;
    let rowPrice = 0;
    if(req.session.cart){
        req.session.cart[index].quantity = quantity;
        rowPrice = parseInt(req.session.cart[index].quantity) * parseInt(req.session.cart[index].price)
        for (const cart of req.session.cart){
            totalCart += (parseInt(cart.quantity) * parseInt(cart.price))
        }
    }
    res.render('frontend/_partials/mini-cart', {sessionCart: req.session.cart}, function (err, html) {
        let response = {
            miniCart:html,
            totalCart: totalCart,
            rowPrice: rowPrice,
        }
        res.json(response);
    })

};

exports.cartPage = async (req, res, next) => {
    res.locals.title = "Giỏ hàng";
    res.render('frontend/cart', {  });
};

exports.checkoutPage = async (req, res, next) => {
    res.locals.title = "Thanh toán";
    let totalItemPrice = 0;
    let shipPrice = 0;
    let totalOrderPrice = 0;
    if(!req.session.user){
        res.redirect("/dang-nhap?url=/thanh-toan");
    }
    if(req.session.cart){
        for (const cart of req.session.cart){
            totalItemPrice += (parseInt(cart.quantity) * parseInt(cart.price))
        }
    }else{
        res.redirect("/san-pham")
    }
    totalOrderPrice = totalItemPrice + shipPrice;
    res.locals.title = "Thanh toán";
    res.render('frontend/checkout', { totalItemPrice:totalItemPrice,shipPrice:shipPrice,totalOrderPrice:totalOrderPrice});
};


exports.order = async (req, res, next) => {
    if(!req.session.user){
        res.redirect("/dang-nhap?url=/thanh-toan");
    }
    let totalItemPrice = 0;
    let shipPrice = 0;
    let orderItems = [];
    let productStock = {};
    if(req.session.cart){
        for (const cart of req.session.cart){
            totalItemPrice += (parseInt(cart.quantity) * parseInt(cart.price))
            orderItems.push({
                order_id: null,
                product_id: cart.product_id,
                product_name: cart.name,
                product_slug: cart.slug,
                product_price: cart.price,
                product_image: cart.image,
                product_attribute: cart.attributes,
                quantity: cart.quantity,
            });
            if(!productStock[cart.product_id]){
                productStock[cart.product_id] = {
                    stock: parseInt(cart.stock),
                    quantity: cart.quantity,
                    name: cart.name,
                }
            }else{
                productStock[cart.product_id].quantity += cart.quantity;
            }
            if(productStock[cart.product_id].quantity > productStock[cart.product_id].stock){
                res.locals.error = "Số lượng sản phẩm <b>" + cart.name +"</b> trong kho không đủ";
                return res.render('frontend/checkout',{});
            }
        }
    }else{
        res.redirect("/san-pham");
    }

    //Check required
    if(!req.body.first_name){
        res.locals.error = "Họ và Tên Đệm không được bỏ trống";
        return res.render('frontend/checkout',{});
    }
    if(!req.body.last_name){
        res.locals.error = "Tên không được bỏ trống";
        return res.render('frontend/checkout',{});
    }
    if(!req.body.address){
        res.locals.error = "Địa chỉ không được bỏ trống";
        return res.render('frontend/checkout',{});
    }
    if(!req.body.phone_number){
        res.locals.error = "Số điện thoại không được bỏ trống";
        return res.render('frontend/checkout',{});
    }
    if(!req.body.payment_method){
        res.locals.error = "Vui lòng chọn hình thức thanh toán";
        return res.render('frontend/checkout',{});
    }



    let order_id = 0;
    let order_number = moment().format("DDMMYYYY") + '-' + moment().format("HHmm");
    let orderData = {
        order_number : order_number,
        user_id : req.session.user ? req.session.user.id : null,
        item_price : totalItemPrice,
        total_price : totalItemPrice + shipPrice,
        payment_method : req.body.payment_method,
        ship_method : null,
        ship_price : shipPrice,
        first_name :  req.body.first_name,
        last_name :  req.body.last_name,
        address :  req.body.address,
        phone_number :  req.body.phone_number,
        status : "Chờ xác nhận",
    };
    //Create Order
    await Order.create(orderData)
        .then(data => {
            order_id = data.id;
            for (let i = 0; i < orderItems.length; i++){
                orderItems[i].order_id = order_id;
            }
            OrderItem.bulkCreate(orderItems).then(data => {
                req.session.cart = null;
                res.redirect(`/don-dat-hang/${order_number}`)
            })
                .catch(error => {
                    res.locals.error = "Có lỗi xảy ra không thể thanh toán, Vui lòng thử lại sau ít phút";
                    return res.render('frontend/checkout',{totalItemPrice:totalItemPrice,shipPrice:shipPrice,totalOrderPrice:totalOrderPrice});
                })
        })
        .catch(err => {
            res.locals.error = "Có lỗi xảy ra không thể thanh toán, Vui lòng thử lại sau ít phút 1";
            return res.render('frontend/checkout',{totalItemPrice:totalItemPrice,shipPrice:shipPrice,totalOrderPrice:totalOrderPrice});
        });

};
exports.orderdetail = async (req, res, next) => {
    let orderNumber = req.params.order_number;
    res.locals.title = "Chi tiết đơn hàng " + orderNumber;
    await Order.findOne({
        include:[
            {
                model: db.OrderItem,
                as: 'items'
            },
            {
                model: db.user,
                as: 'user'
            }
        ],
        where : {
            order_number: orderNumber
        }
    })
        .then(data => {
            //res.json(data);
            return res.render('frontend/order',{item:data,orderNumber:orderNumber});
        })
        .catch(error => {
            //res.send(error)
            res.locals.error = "Không thể tải đơn hàng " + orderNumber;
            return res.render('frontend/order',{orderNumber:orderNumber});
        })
};



