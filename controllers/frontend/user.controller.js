const bcrypt = require("bcrypt");
const db = require("../../models");
const User = db.user;
const Op = db.Sequelize.Op;

exports.loginAdmin = async (req, res) => {
    req.session.views = 1;
    const body = req.body;
    const user = await User.findOne({
        where : {
            username: body.username,
            is_admin : true,
            status : 'Active',
        },
        include:[
            {
                model:db.Role,
                as: "role"
            }
        ]
    });
    if (user) {
        const validPassword = await bcrypt.compare(body.password, user.password);
        if (validPassword) {
            req.session.user = user;
            res.redirect('/admin');
        } else {
            res.redirect('/login');
        }

    } else {
        res.redirect('/login');
    }
};
exports.loginFrontEnd = async (req, res) => {
    const body = req.body;
    const user = await User.findOne({
        where : {
            username: body.username,
            status : 'Active',
        },
        include:[
            {
                model:db.Role,
                as: "role"
            }
        ]
    });
    if (user) {
        const validPassword = await bcrypt.compare(body.password, user.password);
        if (validPassword) {
            req.session.user = user;
            if(req.query.url){
                res.redirect(req.query.url);
            }else{
                res.redirect('/');
            }

        } else {
            res.locals.error = "Mật khẩu không chính xác";
            return res.render('frontend/login',{});
        }

    } else {
        res.locals.error = "Tài khoản không tồn tại";
        return res.render('frontend/login',{});
    }
};
exports.logout = async (req, res) => {
    req.session.user = null;
    res.redirect('/');
};
exports.register = async (req, res) => {
    //page setting
    res.locals.title = "Đăng ký";

    //Check required
    if(!req.body.username){
        res.locals.error = "Tên tài khoản không được bỏ trống";
        return res.render('frontend/register',{});
    }
    if(!req.body.password){
        res.locals.error = "Tên tài khoản không được bỏ trống";
        return res.render('frontend/register',{});
    }
    if(req.body.password !== req.body.repassword){
        res.locals.error = "Nhập lại mật khẩu không chính xác";
        return res.render('frontend/register',{});
    }
    if(!req.body.first_name){
        res.locals.error = "Họ và Tên Đệm không được bỏ trống";
        return res.render('frontend/register',{});
    }
    if(!req.body.last_name){
        res.locals.error = "Tên không được bỏ trống";
        return res.render('frontend/register',{});
    }
    if(!req.body.address){
        res.locals.error = "Địa chỉ không được bỏ trống";
        return res.render('frontend/register',{});
    }
    if(!req.body.phone_number){
        res.locals.error = "Số điện thoại không được bỏ trống";
        return res.render('frontend/register',{});
    }
    const salt = await bcrypt.genSalt(10);
    let password = await bcrypt.hash(req.body.password, salt);
    let userData = {
        first_name: req.body.first_name,
        last_name: req.body.last_name,
        full_name: req.body.first_name + ' ' + req.body.last_name,
        username: req.body.username,
        password: password,
        phone_number: req.body.phone_number,
        address: req.body.address,
        status: "Active",
        is_admin: false,
    };

    const userExist = await User.findOne({ where: { username: userData.username } });
    if (userExist === null) {
        User.create(userData)
            .then(data => {
                res.redirect('/tai-khoan');
            })
            .catch(err => {
                res.locals.error = "Có lỗi xảy ra không thể đăng nhập";
                return res.render('frontend/register',{});
            });
    } else {
        res.locals.error = "Tài khoản đã tồn tại";
        return res.render('frontend/register',{});
    }

};
exports.profile = async (req, res) => {
    if(!req.session.user){
        res.redirect('/dang-nhap');
    }

    //page setting
    res.locals.title = "Tài khoản của tôi";

    //GET User
    User.findByPk(req.session.user.id,{
        // include : [
        //     {
        //         model: db.Order,
        //         as: "orders",
        //         include:[
        //             {
        //                 model: db.OrderItem,
        //                 as: 'items'
        //             }
        //         ]
        //     }
        // ]
    })
        .then(data => {
            return res.render('frontend/profile',{item:data});
        })
        .catch(err => {
            res.locals.error = `Không load được thông tin người dùng`;
            res.render('/dang-nhap',{userId : user_id});
        });

    //return res.render('frontend/profile',{});
};

exports.updateProfile = async (req, res) => {
    if(!req.session.user){
        res.redirect('/dang-nhap');
    }
    //page setting
    res.locals.title = "Tài khoản của tôi";

    //Check required
    if(!req.body.first_name){
        res.locals.error = "Họ và Tên Đệm không được bỏ trống";
        return res.render('frontend/profile',{});
    }
    if(!req.body.last_name){
        res.locals.error = "Tên không được bỏ trống";
        return res.render('frontend/profile',{});
    }
    if(!req.body.address){
        res.locals.error = "Địa chỉ không được bỏ trống";
        return res.render('frontend/profile',{});
    }
    if(!req.body.phone_number){
        res.locals.error = "Số điện thoại không được bỏ trống";
        return res.render('frontend/profile',{});
    }

    let userData = {
        first_name: req.body.first_name,
        last_name: req.body.last_name,
        full_name: req.body.first_name + ' ' + req.body.last_name,
        phone_number: req.body.phone_number,
        address: req.body.address,
    };
    let user_id = req.session.user.id;

     await User.update(userData, {
        where: { id: user_id }
    })
        .then(async num => {
            console.log(num)
            if (num == 1) {
                res.locals.success_message = "Đã cập nhật thành công";
            } else {
                res.locals.error = "Không thể cập nhật thông tin. Vui lòng thử lại sau";
            }
        })
        .catch(err => {
            res.locals.error = `Có lỗi xảy ra. Không thể cập nhật người dùng`;
        });
    User.findByPk(user_id,{
        include:[
            {
                model:db.Role,
                as: "role"
            }
        ]
    })
        .then(async data =>{
            req.session.user = data;
            res.locals.userLogged = req.session.user || null;
            res.render('frontend/profile',{});
        })
        .catch(err => {
            res.render('frontend/profile',{});
        })
};