PGDMP     1    5    	            y            shop    12.7    12.7 k    �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            �           1262    16393    shop    DATABASE     �   CREATE DATABASE shop WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'English_United States.1252' LC_CTYPE = 'English_United States.1252';
    DROP DATABASE shop;
                postgres    false            �            1259    16394    attribute_items    TABLE       CREATE TABLE public.attribute_items (
    id integer NOT NULL,
    attribute_id bigint,
    name character varying(255),
    background character varying(255),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);
 #   DROP TABLE public.attribute_items;
       public         heap    postgres    false            �            1259    16400    attribute_items_id_seq    SEQUENCE     �   CREATE SEQUENCE public.attribute_items_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.attribute_items_id_seq;
       public          postgres    false    202            �           0    0    attribute_items_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.attribute_items_id_seq OWNED BY public.attribute_items.id;
          public          postgres    false    203            �            1259    16402 
   attributes    TABLE     �   CREATE TABLE public.attributes (
    id integer NOT NULL,
    name character varying(255),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);
    DROP TABLE public.attributes;
       public         heap    postgres    false            �            1259    16405    attributes_id_seq    SEQUENCE     �   CREATE SEQUENCE public.attributes_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.attributes_id_seq;
       public          postgres    false    204            �           0    0    attributes_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.attributes_id_seq OWNED BY public.attributes.id;
          public          postgres    false    205            �            1259    16407    blogs    TABLE     M  CREATE TABLE public.blogs (
    id integer NOT NULL,
    title character varying(255),
    slug character varying(255),
    content text,
    description text,
    image character varying(255),
    status character varying(255),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);
    DROP TABLE public.blogs;
       public         heap    postgres    false            �            1259    16413    blogs_id_seq    SEQUENCE     �   CREATE SEQUENCE public.blogs_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.blogs_id_seq;
       public          postgres    false    206            �           0    0    blogs_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public.blogs_id_seq OWNED BY public.blogs.id;
          public          postgres    false    207            �            1259    16415    media    TABLE     �   CREATE TABLE public.media (
    id integer NOT NULL,
    name character varying(255),
    url character varying(255),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);
    DROP TABLE public.media;
       public         heap    postgres    false            �            1259    16421    media_id_seq    SEQUENCE     �   CREATE SEQUENCE public.media_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.media_id_seq;
       public          postgres    false    208            �           0    0    media_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public.media_id_seq OWNED BY public.media.id;
          public          postgres    false    209            �            1259    16697    order-items    TABLE     �  CREATE TABLE public."order-items" (
    id integer NOT NULL,
    order_id bigint,
    product_id bigint,
    product_name character varying(255),
    product_slug character varying(255),
    product_price bigint,
    product_image text,
    product_attribute json,
    quantity bigint,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);
 !   DROP TABLE public."order-items";
       public         heap    postgres    false            �            1259    16695    order-items_id_seq    SEQUENCE     �   CREATE SEQUENCE public."order-items_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public."order-items_id_seq";
       public          postgres    false    229            �           0    0    order-items_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public."order-items_id_seq" OWNED BY public."order-items".id;
          public          postgres    false    228            �            1259    16664    orders    TABLE     (  CREATE TABLE public.orders (
    id integer NOT NULL,
    order_number character varying(255),
    user_id bigint,
    item_price bigint,
    total_price bigint,
    payment_method character varying(255),
    ship_method character varying(255),
    ship_price bigint,
    first_name character varying(255),
    last_name character varying(255),
    address character varying(255),
    phone_number character varying(255),
    status character varying(255),
    "createdAt" timestamp with time zone,
    "updatedAt" timestamp with time zone NOT NULL
);
    DROP TABLE public.orders;
       public         heap    postgres    false            �            1259    16662    orders_id_seq    SEQUENCE     �   CREATE SEQUENCE public.orders_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.orders_id_seq;
       public          postgres    false    225            �           0    0    orders_id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE public.orders_id_seq OWNED BY public.orders.id;
          public          postgres    false    224            �            1259    16423    product_attribute_items    TABLE     �   CREATE TABLE public.product_attribute_items (
    id integer NOT NULL,
    product_attribute_id bigint,
    attribute_item_id bigint,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);
 +   DROP TABLE public.product_attribute_items;
       public         heap    postgres    false            �            1259    16426    product_attribute_items_id_seq    SEQUENCE     �   CREATE SEQUENCE public.product_attribute_items_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 5   DROP SEQUENCE public.product_attribute_items_id_seq;
       public          postgres    false    210            �           0    0    product_attribute_items_id_seq    SEQUENCE OWNED BY     a   ALTER SEQUENCE public.product_attribute_items_id_seq OWNED BY public.product_attribute_items.id;
          public          postgres    false    211            �            1259    16428    product_attributes    TABLE     �   CREATE TABLE public.product_attributes (
    id integer NOT NULL,
    product_id bigint,
    attribute_id bigint,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);
 &   DROP TABLE public.product_attributes;
       public         heap    postgres    false            �            1259    16431    product_attributes_id_seq    SEQUENCE     �   CREATE SEQUENCE public.product_attributes_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 0   DROP SEQUENCE public.product_attributes_id_seq;
       public          postgres    false    212            �           0    0    product_attributes_id_seq    SEQUENCE OWNED BY     W   ALTER SEQUENCE public.product_attributes_id_seq OWNED BY public.product_attributes.id;
          public          postgres    false    213            �            1259    16433    product_categories    TABLE     $  CREATE TABLE public.product_categories (
    id integer NOT NULL,
    name character varying(255),
    slug character varying(255),
    description text,
    image character varying(255),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);
 &   DROP TABLE public.product_categories;
       public         heap    postgres    false            �            1259    16439    product_categories_id_seq    SEQUENCE     �   CREATE SEQUENCE public.product_categories_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 0   DROP SEQUENCE public.product_categories_id_seq;
       public          postgres    false    214            �           0    0    product_categories_id_seq    SEQUENCE OWNED BY     W   ALTER SEQUENCE public.product_categories_id_seq OWNED BY public.product_categories.id;
          public          postgres    false    215            �            1259    16441    product_categories_maps    TABLE     �   CREATE TABLE public.product_categories_maps (
    id integer NOT NULL,
    product_id bigint,
    product_category_id bigint,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);
 +   DROP TABLE public.product_categories_maps;
       public         heap    postgres    false            �            1259    16444    product_categories_maps_id_seq    SEQUENCE     �   CREATE SEQUENCE public.product_categories_maps_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 5   DROP SEQUENCE public.product_categories_maps_id_seq;
       public          postgres    false    216            �           0    0    product_categories_maps_id_seq    SEQUENCE OWNED BY     a   ALTER SEQUENCE public.product_categories_maps_id_seq OWNED BY public.product_categories_maps.id;
          public          postgres    false    217            �            1259    16446    product_images    TABLE     �   CREATE TABLE public.product_images (
    id integer NOT NULL,
    product_id bigint,
    image character varying(255),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);
 "   DROP TABLE public.product_images;
       public         heap    postgres    false            �            1259    16449    product_images_id_seq    SEQUENCE     �   CREATE SEQUENCE public.product_images_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.product_images_id_seq;
       public          postgres    false    218            �           0    0    product_images_id_seq    SEQUENCE OWNED BY     O   ALTER SEQUENCE public.product_images_id_seq OWNED BY public.product_images.id;
          public          postgres    false    219            �            1259    16451    products    TABLE     �  CREATE TABLE public.products (
    id integer NOT NULL,
    name character varying(255),
    slug character varying(255),
    sku character varying(255),
    description text,
    price bigint,
    sale_price bigint,
    category_id bigint,
    label character varying(255),
    image text,
    status character varying(255),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    stock bigint
);
    DROP TABLE public.products;
       public         heap    postgres    false            �            1259    16457    products_id_seq    SEQUENCE     �   CREATE SEQUENCE public.products_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.products_id_seq;
       public          postgres    false    220            �           0    0    products_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE public.products_id_seq OWNED BY public.products.id;
          public          postgres    false    221            �            1259    16686    roles    TABLE     �   CREATE TABLE public.roles (
    id integer NOT NULL,
    name character varying(255),
    actions json,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);
    DROP TABLE public.roles;
       public         heap    postgres    false            �            1259    16684    roles_id_seq    SEQUENCE     �   CREATE SEQUENCE public.roles_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.roles_id_seq;
       public          postgres    false    227            �           0    0    roles_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public.roles_id_seq OWNED BY public.roles.id;
          public          postgres    false    226            �            1259    16459    users    TABLE     �  CREATE TABLE public.users (
    id integer NOT NULL,
    first_name character varying(255),
    last_name character varying(255),
    full_name character varying(255),
    username character varying(255),
    password character varying(255),
    phone_number character varying(255),
    address text,
    status character varying(255),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    is_admin boolean,
    role_id bigint
);
    DROP TABLE public.users;
       public         heap    postgres    false            �            1259    16465    users_id_seq    SEQUENCE     �   CREATE SEQUENCE public.users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.users_id_seq;
       public          postgres    false    222            �           0    0    users_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;
          public          postgres    false    223            �
           2604    16467    attribute_items id    DEFAULT     x   ALTER TABLE ONLY public.attribute_items ALTER COLUMN id SET DEFAULT nextval('public.attribute_items_id_seq'::regclass);
 A   ALTER TABLE public.attribute_items ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    203    202            �
           2604    16468    attributes id    DEFAULT     n   ALTER TABLE ONLY public.attributes ALTER COLUMN id SET DEFAULT nextval('public.attributes_id_seq'::regclass);
 <   ALTER TABLE public.attributes ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    205    204            �
           2604    16469    blogs id    DEFAULT     d   ALTER TABLE ONLY public.blogs ALTER COLUMN id SET DEFAULT nextval('public.blogs_id_seq'::regclass);
 7   ALTER TABLE public.blogs ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    207    206            �
           2604    16470    media id    DEFAULT     d   ALTER TABLE ONLY public.media ALTER COLUMN id SET DEFAULT nextval('public.media_id_seq'::regclass);
 7   ALTER TABLE public.media ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    209    208            �
           2604    16700    order-items id    DEFAULT     t   ALTER TABLE ONLY public."order-items" ALTER COLUMN id SET DEFAULT nextval('public."order-items_id_seq"'::regclass);
 ?   ALTER TABLE public."order-items" ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    229    228    229            �
           2604    16667 	   orders id    DEFAULT     f   ALTER TABLE ONLY public.orders ALTER COLUMN id SET DEFAULT nextval('public.orders_id_seq'::regclass);
 8   ALTER TABLE public.orders ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    224    225    225            �
           2604    16471    product_attribute_items id    DEFAULT     �   ALTER TABLE ONLY public.product_attribute_items ALTER COLUMN id SET DEFAULT nextval('public.product_attribute_items_id_seq'::regclass);
 I   ALTER TABLE public.product_attribute_items ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    211    210            �
           2604    16472    product_attributes id    DEFAULT     ~   ALTER TABLE ONLY public.product_attributes ALTER COLUMN id SET DEFAULT nextval('public.product_attributes_id_seq'::regclass);
 D   ALTER TABLE public.product_attributes ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    213    212            �
           2604    16473    product_categories id    DEFAULT     ~   ALTER TABLE ONLY public.product_categories ALTER COLUMN id SET DEFAULT nextval('public.product_categories_id_seq'::regclass);
 D   ALTER TABLE public.product_categories ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    215    214            �
           2604    16474    product_categories_maps id    DEFAULT     �   ALTER TABLE ONLY public.product_categories_maps ALTER COLUMN id SET DEFAULT nextval('public.product_categories_maps_id_seq'::regclass);
 I   ALTER TABLE public.product_categories_maps ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    217    216            �
           2604    16475    product_images id    DEFAULT     v   ALTER TABLE ONLY public.product_images ALTER COLUMN id SET DEFAULT nextval('public.product_images_id_seq'::regclass);
 @   ALTER TABLE public.product_images ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    219    218            �
           2604    16476    products id    DEFAULT     j   ALTER TABLE ONLY public.products ALTER COLUMN id SET DEFAULT nextval('public.products_id_seq'::regclass);
 :   ALTER TABLE public.products ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    221    220            �
           2604    16689    roles id    DEFAULT     d   ALTER TABLE ONLY public.roles ALTER COLUMN id SET DEFAULT nextval('public.roles_id_seq'::regclass);
 7   ALTER TABLE public.roles ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    226    227    227            �
           2604    16477    users id    DEFAULT     d   ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);
 7   ALTER TABLE public.users ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    223    222            �          0    16394    attribute_items 
   TABLE DATA           g   COPY public.attribute_items (id, attribute_id, name, background, "createdAt", "updatedAt") FROM stdin;
    public          postgres    false    202   �       �          0    16402 
   attributes 
   TABLE DATA           H   COPY public.attributes (id, name, "createdAt", "updatedAt") FROM stdin;
    public          postgres    false    204   G�       �          0    16407    blogs 
   TABLE DATA           o   COPY public.blogs (id, title, slug, content, description, image, status, "createdAt", "updatedAt") FROM stdin;
    public          postgres    false    206   ��       �          0    16415    media 
   TABLE DATA           H   COPY public.media (id, name, url, "createdAt", "updatedAt") FROM stdin;
    public          postgres    false    208   ѱ       �          0    16697    order-items 
   TABLE DATA           �   COPY public."order-items" (id, order_id, product_id, product_name, product_slug, product_price, product_image, product_attribute, quantity, "createdAt", "updatedAt") FROM stdin;
    public          postgres    false    229   ��       �          0    16664    orders 
   TABLE DATA           �   COPY public.orders (id, order_number, user_id, item_price, total_price, payment_method, ship_method, ship_price, first_name, last_name, address, phone_number, status, "createdAt", "updatedAt") FROM stdin;
    public          postgres    false    225   M�       �          0    16423    product_attribute_items 
   TABLE DATA           x   COPY public.product_attribute_items (id, product_attribute_id, attribute_item_id, "createdAt", "updatedAt") FROM stdin;
    public          postgres    false    210   2�       �          0    16428    product_attributes 
   TABLE DATA           d   COPY public.product_attributes (id, product_id, attribute_id, "createdAt", "updatedAt") FROM stdin;
    public          postgres    false    212   N�       �          0    16433    product_categories 
   TABLE DATA           j   COPY public.product_categories (id, name, slug, description, image, "createdAt", "updatedAt") FROM stdin;
    public          postgres    false    214   ��       �          0    16441    product_categories_maps 
   TABLE DATA           p   COPY public.product_categories_maps (id, product_id, product_category_id, "createdAt", "updatedAt") FROM stdin;
    public          postgres    false    216   �       �          0    16446    product_images 
   TABLE DATA           Y   COPY public.product_images (id, product_id, image, "createdAt", "updatedAt") FROM stdin;
    public          postgres    false    218   ��       �          0    16451    products 
   TABLE DATA           �   COPY public.products (id, name, slug, sku, description, price, sale_price, category_id, label, image, status, "createdAt", "updatedAt", stock) FROM stdin;
    public          postgres    false    220   ��       �          0    16686    roles 
   TABLE DATA           L   COPY public.roles (id, name, actions, "createdAt", "updatedAt") FROM stdin;
    public          postgres    false    227   �       �          0    16459    users 
   TABLE DATA           �   COPY public.users (id, first_name, last_name, full_name, username, password, phone_number, address, status, "createdAt", "updatedAt", is_admin, role_id) FROM stdin;
    public          postgres    false    222   ��       �           0    0    attribute_items_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.attribute_items_id_seq', 17, true);
          public          postgres    false    203            �           0    0    attributes_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.attributes_id_seq', 2, true);
          public          postgres    false    205            �           0    0    blogs_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('public.blogs_id_seq', 5, true);
          public          postgres    false    207            �           0    0    media_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.media_id_seq', 70, true);
          public          postgres    false    209            �           0    0    order-items_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public."order-items_id_seq"', 3, true);
          public          postgres    false    228            �           0    0    orders_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.orders_id_seq', 2, true);
          public          postgres    false    224            �           0    0    product_attribute_items_id_seq    SEQUENCE SET     N   SELECT pg_catalog.setval('public.product_attribute_items_id_seq', 223, true);
          public          postgres    false    211            �           0    0    product_attributes_id_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('public.product_attributes_id_seq', 35, true);
          public          postgres    false    213            �           0    0    product_categories_id_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('public.product_categories_id_seq', 35, true);
          public          postgres    false    215            �           0    0    product_categories_maps_id_seq    SEQUENCE SET     M   SELECT pg_catalog.setval('public.product_categories_maps_id_seq', 87, true);
          public          postgres    false    217            �           0    0    product_images_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.product_images_id_seq', 866, true);
          public          postgres    false    219            �           0    0    products_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.products_id_seq', 17, true);
          public          postgres    false    221            �           0    0    roles_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('public.roles_id_seq', 5, true);
          public          postgres    false    226            �           0    0    users_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('public.users_id_seq', 8, true);
          public          postgres    false    223            �
           2606    16482 $   attribute_items attribute_items_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.attribute_items
    ADD CONSTRAINT attribute_items_pkey PRIMARY KEY (id);
 N   ALTER TABLE ONLY public.attribute_items DROP CONSTRAINT attribute_items_pkey;
       public            postgres    false    202            �
           2606    16484    attributes attributes_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.attributes
    ADD CONSTRAINT attributes_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.attributes DROP CONSTRAINT attributes_pkey;
       public            postgres    false    204            �
           2606    16486    blogs blogs_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.blogs
    ADD CONSTRAINT blogs_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.blogs DROP CONSTRAINT blogs_pkey;
       public            postgres    false    206            �
           2606    16488    media media_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.media
    ADD CONSTRAINT media_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.media DROP CONSTRAINT media_pkey;
       public            postgres    false    208            �
           2606    16705    order-items order-items_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public."order-items"
    ADD CONSTRAINT "order-items_pkey" PRIMARY KEY (id);
 J   ALTER TABLE ONLY public."order-items" DROP CONSTRAINT "order-items_pkey";
       public            postgres    false    229            �
           2606    16672    orders orders_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public.orders
    ADD CONSTRAINT orders_pkey PRIMARY KEY (id);
 <   ALTER TABLE ONLY public.orders DROP CONSTRAINT orders_pkey;
       public            postgres    false    225            �
           2606    16490 4   product_attribute_items product_attribute_items_pkey 
   CONSTRAINT     r   ALTER TABLE ONLY public.product_attribute_items
    ADD CONSTRAINT product_attribute_items_pkey PRIMARY KEY (id);
 ^   ALTER TABLE ONLY public.product_attribute_items DROP CONSTRAINT product_attribute_items_pkey;
       public            postgres    false    210            �
           2606    16492 *   product_attributes product_attributes_pkey 
   CONSTRAINT     h   ALTER TABLE ONLY public.product_attributes
    ADD CONSTRAINT product_attributes_pkey PRIMARY KEY (id);
 T   ALTER TABLE ONLY public.product_attributes DROP CONSTRAINT product_attributes_pkey;
       public            postgres    false    212            �
           2606    16494 4   product_categories_maps product_categories_maps_pkey 
   CONSTRAINT     r   ALTER TABLE ONLY public.product_categories_maps
    ADD CONSTRAINT product_categories_maps_pkey PRIMARY KEY (id);
 ^   ALTER TABLE ONLY public.product_categories_maps DROP CONSTRAINT product_categories_maps_pkey;
       public            postgres    false    216            �
           2606    16496 *   product_categories product_categories_pkey 
   CONSTRAINT     h   ALTER TABLE ONLY public.product_categories
    ADD CONSTRAINT product_categories_pkey PRIMARY KEY (id);
 T   ALTER TABLE ONLY public.product_categories DROP CONSTRAINT product_categories_pkey;
       public            postgres    false    214            �
           2606    16498 "   product_images product_images_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.product_images
    ADD CONSTRAINT product_images_pkey PRIMARY KEY (id);
 L   ALTER TABLE ONLY public.product_images DROP CONSTRAINT product_images_pkey;
       public            postgres    false    218            �
           2606    16500    products products_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.products
    ADD CONSTRAINT products_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY public.products DROP CONSTRAINT products_pkey;
       public            postgres    false    220            �
           2606    16694    roles roles_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.roles DROP CONSTRAINT roles_pkey;
       public            postgres    false    227            �
           2606    16502    users users_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.users DROP CONSTRAINT users_pkey;
       public            postgres    false    222                        2606    16503 1   attribute_items attribute_items_attribute_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.attribute_items
    ADD CONSTRAINT attribute_items_attribute_id_fkey FOREIGN KEY (attribute_id) REFERENCES public.attributes(id) ON UPDATE CASCADE ON DELETE CASCADE;
 [   ALTER TABLE ONLY public.attribute_items DROP CONSTRAINT attribute_items_attribute_id_fkey;
       public          postgres    false    204    2791    202                       2606    16706 %   order-items order-items_order_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public."order-items"
    ADD CONSTRAINT "order-items_order_id_fkey" FOREIGN KEY (order_id) REFERENCES public.orders(id) ON UPDATE CASCADE ON DELETE CASCADE;
 S   ALTER TABLE ONLY public."order-items" DROP CONSTRAINT "order-items_order_id_fkey";
       public          postgres    false    225    2811    229                       2606    16508 H   product_categories_maps product_categories_maps_product_category_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.product_categories_maps
    ADD CONSTRAINT product_categories_maps_product_category_id_fkey FOREIGN KEY (product_category_id) REFERENCES public.product_categories(id) ON UPDATE CASCADE ON DELETE CASCADE;
 r   ALTER TABLE ONLY public.product_categories_maps DROP CONSTRAINT product_categories_maps_product_category_id_fkey;
       public          postgres    false    216    214    2801                       2606    16513 ?   product_categories_maps product_categories_maps_product_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.product_categories_maps
    ADD CONSTRAINT product_categories_maps_product_id_fkey FOREIGN KEY (product_id) REFERENCES public.products(id) ON UPDATE CASCADE ON DELETE CASCADE;
 i   ALTER TABLE ONLY public.product_categories_maps DROP CONSTRAINT product_categories_maps_product_id_fkey;
       public          postgres    false    220    216    2807                       2606    16518 -   product_images product_images_product_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.product_images
    ADD CONSTRAINT product_images_product_id_fkey FOREIGN KEY (product_id) REFERENCES public.products(id) ON UPDATE CASCADE ON DELETE CASCADE;
 W   ALTER TABLE ONLY public.product_images DROP CONSTRAINT product_images_product_id_fkey;
       public          postgres    false    2807    218    220            �     x�}�;N�0��s�H[��f�o��h@(�6H��\$.���.��`��:^��'�'�!��?o���t#��l���a��:C׈������sx����.���BEZD���hHو(ٺ5��@'���㯝{SuB*6ug��}Q�9FMRY*�"���b�}'�9J�bH��#��ry
�<A�P��r�@X����<���d�u�?�����YQn�t�#�DV�SYE@fںq���帝�Z�j��!r	:��y���]�]qd��|$�"�I ��s��      �   Q   x�3�άJ�4202�50�54U02�2��24�344�60�#�e�霟�_����H���E�������������Hs� u��      �      x��}]��q�5�+:{A�Aff�{v��2���LŁi�0���in��3ݣ��7W��A&#PCڗ`F!(��������_�Su�S����,?D�2,�;;�s>�T=U�T�������o/�龗/�O�xݞW�v�E�{O���O�Nf޻˳�{��yy�������X���A�-�O���8K���z�����;u��8��'�Y���Q=��yo�?���E^���d�斿��?�'�����y��~g��K?���2��S�%�qk��/�OL�Ir~�~*u�Q�����׹'>%F}�(�yt�/ga2�x�/M.G�szu;n��l����w��wy2~w�]���O.g����:>yg�)��@�*�=�,����w�_���at�g_�p��oz9���5� �X�O�Ɯ<H�E�u�(p���29����׉ؑ�����P>_�V�O�/���<���S����O��"�f������c?����]�Q�{��oo��-u���<Cq��O�h����'F��z)?����+�X�O���jf�T�CL�s1����)�l��8U�_q�(��5W�9�im�;G���$A`C�������HIB��@H����C�]S��⳶h�M�ü����3)\���[�G��ͭ<��׃I���za��������h�w5��yt�ͭ8�g������{�qpp;X�I�6��q0��YP�8Xp��xY��$���2����z�����|?�řM��`˛G�7��L<?�ϣ��f�,J����d*Dy,onM���[j��d1�G��h��bN�IZ�D��]/X�~i����5��yh&�S{����b��l���m���o�8�V������`"&z��l�۲�@�7/���~[-�{�8�����Ŀ�fD?��<�ӓ�/�d�8�Q�'����n{WGs�w�,�1�)T��)��}��7���P��!1���ai�(�W�A���^@jq8��F|'��yp$81���'�~����o�	/2k�ĊHI���Ue�~*����ҽ&B�^'��,i׋k4Y*��-�P�`����V� �f(Ҭ[���c�U���_<=N� ���B���&{ၲd�J-=q^O��ci�a��5�'a'�(3���� }	��+�I�dً���Q�����m����j��h��k��#>�����(��9~�#!<��y�qr9�&�dvE�D�e�A��tw���d���X����9�b��@� ��!�
^~K#���~����Fj�<x��n����Jܛr:Ћ�N(N	�h�ו�����n�ǡX]�O�9O�d��1������`[�LM�ъ�|Ƹ�����Kq<���2Лn>j >n��E�-���2��Hkł���B$��SIC~�\ ~C��_%�>�mPI��U(�5M��G,�1.�x����,�"bNa�>�9�'��XT�q�&by���o��S���P�M��Y*�A��Q���U���*$ w@(/��)E�xw����P̉��\m��� �����`�z��m`������Ӥ>�R4֓T�B�,�0uZ�3�af�'��h9'���R��[?�A�����Z���g����N/�!��ٕi���,#
(���d�c̎����%N�7������8��l�g3} 0 �y���`������(����.��kN%ot�X� ��nb��[^i�n�c��c�ͳ���r�`>S2]R#
��Q�D�1J1����'`ф|�8#�91c�!�D�(	0�0�W#C)�8�#aŤf�!@������~[���<�c��Ђc�)H��F��F�t���-t�h�3--���x�]YS��ᆨ7c;Q�FE* h]�+S@�Q& j�}��7Kq.j�#�(�elC��q ����O?U�r�ve�2TCoB�Q���_bdVG����r��8��ފ�I#ǘĲNdGȎK�� $�����Q^c2��d#S+<|͒�)lplSvl��K�
B:� ��U+�}͎DQ��c�5��m�2�*�O������6�j�/Q�2��!��ὗ��,��h��(��d���~G�coM��l�)?
ms��kc/{�K������K�8�1f�|��sZ�W0�V
%�I��	��3n���K]�́���t��0|n����N4�O��Y�}[pb�	���*������G3+��2����}�Bm�s�M���C���|ri�1å��|��,K�o�+�]�zDۙ^`�[V�
	E�Sz�ݪ�3\� ��PXit~#�et^�_ϼɑ�83ָ�t,E��Y}�k�������r{T�@���؇O�o�ͥ�Q2�͘�V���5��N����% ƹ�C��8N�v�{�4����K9�I����ҫ�% u\4~��n�y��j�T��1�X�σ�wK�N�_פ��q�vKp�]�_�~|�`{9��؞�n�?�����@�~�3lu[�^��i��f�FȾ��e��ނ�DU
^P��R<_�
����V���E'���u@XPV~�F'��M#���ZN����8��
��(��	/�㥉YHA�r�FTQ��������9��b�`j����>�<�lQ���H�y��{��h-2�	��p���
`������B'ʍ-���`��EfKc���=0/n��Y'2��N�Y4φ2�B�/G4�T�`I�A
͋���!�L&u���ҹi[P��	�����]��~;�?S"�~mD~��ɭ�ӱ�>K-�=��K+�J�K�| ����o0�i\	>��kc���͇j<��σ%�{���/�yDtL,�x����L���p�!&+�'�5���w�ZZQ���l����a�L+�C�Mr�x�3��:�� �b�����˛����%ς�3��Y��f
�ͬ���U1g�'�t��)B���zӳ�P�2�)@޳O%C��?S̨%�D|H@���^��!(�_	1F�!�{x�YV�N����E�Z{�9�g�g�ϳ�TS�c�����g��z�w���C`�Qظ�M��K?^�&�"�ڪ�oൺ���n�����9�~��m�v�;�a~�F��M��8�VS��ώ����K���3)��%��~�]Jɻ{)�#�V ��4X���������T�[���~Ge/	;8N�����b�ND�~{�����b�"2ye|��.��k9u��Q+Ѳ�i��a�5�>�ĕ�J��+y��n�:�H8�%�$�� ui'-Wk-99�X�4�**kƹ�n��cӟG2W��O,/�-�����@T�!%	���8���8&��$w��*q�/���xZ�����k엘�Ǹ�v���'K!#�#�^�4N,��r��C~�!Y�9���❟$z������Ǐyf&
G�YcҚ��"0�Ǘ����b zp�Q���)�sɚ�̰����\j��+�V�)8��yP���:��Lm9j.��-b�^M���oM��B�$4��*آ�lcc���^]���٣���m�V�ER��a�40n���$��gxh<B��)A4hY�F!2@�Ö�_"Q2�
BF:�g�8�*���jx7�+��b#�3#5�
ݡ���쌽͑0�<�H��1�fz��n��J W)�;˾�~ݤ�����@W��@ N��e)Y�R����/�<@z������Ce�8��B��t�+3��^p�.�����	M(E�I�2�d�*O�%^��|�}�Tf�W;�9凙�m�����^���η&���~6TWo��X�$(\���
Y:/Ћޙ�x�67ބ��a�W�&��]��i6�ͦw�'��]dҘm�f��5G��h�M&Ba�>�Ґг��^�Ĩ����2��7镹��;āx|����3�y@�/WVx
�����
YÔ�,�;ԛ�1�:M���%��P�d @�|.���H�3�CS\%�ɂv���0 N�P}�ѓXX
?�W+(@��2�Ӭ��oE ��R��-c~P����z
-;&Gf ehlK��4�5H�e�� �"    ��bd}d�0�b)����i��9��rU�Z��B�^���A��_�w�߈J`QoK�a�D�����J:�K�_t�]�^��;�-������f��R*���:eTj��J�G�%0�iM�͟/ѷ ?���iV���Zl-�9.W82)S�kV 63�Z�@�;&h������d���|�5���fB�e+0�� ���*�;V�V��q��`V(���ӵ����X��'Ex(zH:�S�v"�x����|�7��Ʊ�*�Z�Q�tE�@$�2Ѽ�25[�k������D9Rl�N���J�A��f�YN�oO6��5i�l���J�(����~�)�1�0���Yc�D؏!%���S�[{]��rU�T�C�CLQT |�bS[ӏ!Ҝ�c�2*ՈLZey8�(�T�iՅ���d�Q-G�{\\�hav��6>��?y(�m�����ӕ�_[�Z��9"��P�#UJ�ؒv%�A���yh��p(>�!&� �a�T���ʳ�D>Ę�A�h����?,�Ү�tg�m�y[qWb���`i�_$N�<	]�i�e�!�n�����?{ �YVȒ���*dN��i�~�r�^�nV���?�d�zX�՚�Lں���<���y�X�u�Z��/���"����P��V���@�kx�a?�սkv��A,�֊�-�#>eh�]M�7���U�n�Fl�w�g������2��e��BRɽB�O`YqRh��]=�ԭ4ZHþ��}<ÞF
)RXfg�95�f���f����v�����;�A#
��ZdP_�|��b)Ҋ��̕eS��2+0C����D�WU�N��_����AY�B_��>(���P)݆�����=<'�r��%`]��� ���c~��pW�`�=��bA�Y���b ��;�l��Y���Ɛm� ������ڳs:�E�����!��:�:r�:��,�dEs��o��01Xund����ĕ��\&���\kmϥ�� �68�A�%���/��xk�/ԃ��tc������^g�$$�&՞��w:`0~ �NH��R|Y3IL�[^���c5	��1��U�\��J�3���!�<��
������e�2e�w����i����N�>I�ϭ�L�mRf@*~�%����}���
_U�e7pA3&�E�K��8r��}��.J^�܎r�/�w�^'��Z�d��|ʪ�(�BD>��:�����8�,���Ғ�Ĉ�EX/'��դ�5��2r��{�^4Di�&��J#1pT@g�v*r�)��O�c��*���:X���ti)�JH�.�[3a��f���Ld���/�D�Qm���7���p�O����iʡ����L�$�Y�&+���T��QN4�0>���|��>%�;ŀ _\�tpx��QAkMv�^�k��lV���RVT*�`x�R��ؔ|D�m��E.�FY�c�����[���&z�x���f�Ro8���_9	A���  uh	�T؈��bu��*��R��e�3HO�s����v�3�����ꀘS��G���Z�z����U<�s-���վ�O��;�OA���>�'⇭�����'xN�m��0ͳ2YM�Zۥ^��6��hF@٘R��
��&+�y���l�� �s��U4r��r~VGuf7x�^��Uj�f��\8��<�*�໣sd1�^SP�R^�P0%��:e"yH�e[ՍhyU�Z�bk�bӯ������X.���u}R�ó�a�E�UZ&�Nq~���zi�\�;VMc�P����w�l:���yw�C�
�0k��X�6^���Y�8�)	iT9�柇�4 �q���Ň �����ް�{*>�����w�]���܄]*ѣ���Ǯ���UX��r	��ʘ�Z)RA���2��4#dGZ=q��n�-׿d�����V�+��s��P.������9�ٮvm�ٌGI��B���3k��^�:�S������I���}�1FF5b}V
��\!�	������ ��P��_��&�ȱ�L\�Z�~-���sE��3��V�u<�VSu���'4�O��D3��|k��l�x�t��4�v�O�
�� �T�~G�*�%�:@U�BGf/HǞ�ȼ�
�\��n�np唷>(�ITisGgt![S�v-�!�l"���N�sK��
e����߻#=�&�S��z��q׬� �V�ɳ��9|�.FL4x�1XR�!��[���㣊rVU�9E��"r����p��6�X�z� 	^n}����	�4&S+�Kߞkf�Y�:�"i�+�m�\'�H</�6���3@�Mf�A٥����L��ٿA��ٿCI��#��!�>�rp�ց?��G�q�����bŷx����6Ύe}��g�
�'���Vr�����>���ކ~ ��>;.�j�9�0�b����������;���6�z�oG��鶇��v�;��Acg�u4�h�
3?Ğot.���-\�L�D�a��~�=ﯗgǖ��4B�8�O�r�û�K��`)P���a6S�̼L���j�J�&v�"�KA�1N�~�Ce��Fh�ȼY]u�'����&�e=\&&�Tl`��ʸ�[�9�,�DYӢ�d��`�VG�1albz�H��ܔ�U=4y�\m[YL�D�k�����^8B(��]ɰ<��jᨢ"ό(6���)����>�.]�d#;�[a��B��I=̻n.�o-|1���H����F����q}�@@�ܥ�-}��X�Н5re�ͅB*�Z�&�C %��)��S�U(�@�̦� 6'��r�s� LtklT�1UP-z�]��D�kx?rL�NOY5�H7Q��t�\�M�O*d�T�Y����Iޥ�����,���c����)��*(R��ՈЀ ��Xm9���bEFUW^�����fy�d��t覻b� OP������'��7���5���
�Q�R9Ime�i�P1��,�h����m����������)�A5�&}{��!�Q�Z`b�z��Q����)�a�	�S�W����H"MA���E���}3�U��Z�����cP�rO8c*��ݥ�V����u�@��P���6Z��-?�JTd���u��5�S_Ӈa"�o*~L�VC�T���,EBK��
ԃ�丒Xh�m�~��kU	8��H�P#�j�{�0UICG�����i�<��VRFƬ��FI����d
�A�QW�zS��Rek5ɑ>��(�&��|��6��K�Y�뺸�.l�@�bLʨ(���������s���>T�[U�E�Xb#��y_M�􊮨o>�4�Z�$>w���խ�ʻᾍ�S���Ms�q��N4���S�H���܎��W���"��,��[W�}��@k�%�b�iL�oG�y���8D�]�
�/���/�"n�ঁ��d�`S6��c��[ɬY���M��|��e����w��e�t1�0j,����@D�h9aʴ���v��7\���j?�ٟ(��u�0]��Q��y�]s1��a���}uѲ���敕��MV�E�BƦ�?�!�[oT�<@%QVA}-x-��"K �L(H�	D	��Ԥ���ե.2�W�<�f���յy�e��`������ή*�r��)xP<Y"W�9}��q�Z�L�s� a��_�#�����s� �8�,�M�Sl]%�C�e�Ľ}����>9��mV�GD�2^���U�����,��%�v+%����@��W��(*PI�#���)b9��Y��C+$V�1��y������؂�=�9���YHI�V(mRR�oڻ��$�f�P�a
I�09�f���\��.S�na��j��M�ϪƳ2C�u*��EnI4SM�Se�}i!�U,&4i�Z)���a�p�Se��R�
E(�ԩ`!��ܖ�u�(���i=��=�gҖ���+0D�8\tJ�����0�@��Ma��So>7Q�o��j�����̌?�sv^t}�E~aIZ;��/uս�w2y)z(o���b��&�?L�\5 G��� �@䝗]Ҍ��a�[;�s4�`Z��A�x�'DKb[h�{��*hDH�ʣ,��e��I�1���n����4>+�����d^"$�Si|   �2ZP�ˣ쮜jUԫ+�0�U�	�kU�eą����.�*���K�
�1�/Ǘ���>Vw��Ծ�k��9`[��#li�L��>�����ՠZj|d�3�t+� Ǆ���C�����m{���`�I�+����v���7N�nF�U�*��Yg��������kN7�ͦM�/h�/x����|	��`&^&�k������FVñ�gV�d����8�o<��a��ά \��k�rZt30����3r	��g�1���V��(E��*U.˘��غz��ޥ�l���ޓ1kh]��T�p����(����[Gd�Qj?� 2����CJ�Ns��D���s�"«�����;��؎�=L��N9���@@lc>g��C
y�Lq��}�����%b��t+�E�:;Vd,o5��z��J�R�u�+��V�Y�=d+�a�
�;Dc/{oط%2��K�����i��w�!f6cH��"n���"+
�\�}��t7	��Xӟ����t0�{���Y� o��v�V�� ��6��v���%T���c���g�����?=?�ǿ܂��?�%Ɗ���w���ں��<�k�����
�ۗ��U��2f�͘�����;M��r��k�|kup�'gY��d�DX��3�ۊ:O^����j����d3�|��Bs%�lum����S4�m�y�F|*)녊G;�[eK��ő������V����[�S�o�x2W��������X���(��}jo<L`Wqo�!�G�
�]:A�����&�)$�nD�2�Ր���	\*)ӥ��m2O�VB�_���f-<��"R��dL�� S��,3��	[�حS��ɷ�#<2��92aO!�<����1��ĺ��n�� M���޵�A����K��۱�Dg'�q7��ܔ����<5$1�XFvЦ2Ր_�v{�BM�!_�y�,4�Lw+.��)�5�+X/G������T��z�R�}
��6�z]�B:�`z�&� ��ͥ��%q����;~COA�ٮ���6�\%u8A��
��Qaww��/�{����)p`��Ǯx�-�K�w�]�>��U�djq=��Y<Gj�R��F���D8뼼
:�Us�ZJZ_�Ua8�Z�A�9�`�6���W.��e���y�|�����0&�/5χI�P@�7([���h��R/@ȳ�kӒ+���R3U��(�#-���IJYt��<���$jċ\x&a#�r����Z;��p���N�i����a��m��`4���p(��9ju{��xԏ|1�tI�w�9�F�d�*���3��kj/o��!ظ}���G�Q)���Z����c9F�>��bgy՘]�Z|�R���&�zY���r�u×S5[uV�XVՋ�v��6�pB�w���$��=84(�T����s������Z�s=��$Qs[�TG��^_�7�Z��2.9 �����+���H�vJkv��㌚@:;V��ȇ#=�E?�bN�K�W����C[�G�T H�b�,eFm�)M�K�C߇3����j<��o��N����v�m����:�#�B�{�G����zs����v'	�b({8Zt=�${͖?�;ð����n���[�Þpw[�`��7�'�cðQ6/�RޟOK>�������k���hG���d�D��#����{��"����2�W%��J�C\ӹ]��[ãV�R��A0��b$D*^l&m��M�.V-�O߱$�~Ƚ�yPγVZ��:n��&>��D��bZLN�RZ	C���e~;�7���O�0�-%Q�r׬^*��A�b��E�
�꓉��]^�jr�g9�Q�H�dy�+�9|���?v��6��drL�q����%�F7����T�U85U��A�Jܙ�Y�~Ǽ-/k��e�0�!$)r4,�- �6ѕ$�F�Q��'��6�z͔�~�|���*�$1�a?�����[5A�n5�+��}���S�6���������,O�y����nۭnK�����vf��N��A�5ꃖ�w���?�i�����E�{�je�d�H�E������xk�Ψ-V��\�}�߅�T�&E���C���j�N`y��]�%��˩��;f�8�D� M�F�<��gv���sKG���t��B:ZB:�C���m�o���V�����h0v[�`(D&���
D m<F���j@<V�N���x�>O�
e��yJ�f�0�^��-��U7�kP������@CEX�Qb��-��CRf��V�
�2,	]Ӹ_nu�,5�:d��jfL����b���a�I���t�9ge�Ec�#�t��Q5\Y�lgSh�j=�Q�C�2c��g]Ai�*ɴ�n�r��41�,�l4��{���x!f(c�*�&=�eW�o�Gh����_���&4k�QYh�j�Xc-���M#u+�G�keyK��>>��e��+��-��p����eu��v;��q67/\Bd��U��CM��������F-�J�B�f e���5�{���mK{ч�v۝�p8�G�N(���?�
�x���{��`8�Q;X���a���w�Q�=��"�fz�a�錚��@����?�JWk/�7v�i�"$��!Ra!�X��I���;H$8�gT�Z���Y�C%r ;2�c!B6x���-�2I5Iqhu��.�61.�����Ov�+�
�kB�ŦX{��	W�N���Hey���Y�NK�X��hP�i��{a�qwlXqW�#��(�	e	�7$G@�pf��V�M���N0�[A�t��ް�o���ΰ��{8~G�q0�Ga�v�7�F�A����wF�A;��y�_)�7Ū�,5�bF�.���תgԥ욝b)�u���<:���#��˔��h����nO��*�e�U%�9IUfϤayS���ug�}M��vԾW���p��>���8����/z�`�u�D��Y��X�*F�?(/-љ�b�h]n��S�d	�&J��LKm��Ѥ%1�V�Uo��s�;�A���Ñp�à6����v�|�o&�'��`�k5��D��4�7�^�����m�Xʒ��p��i촐����o��'{.      �     x��[�r7}n~��ښ	��[�j����X�Ērٯ_t�$w��YcE.������$.�����v`��Z���z��~y8~����v�� >!\0_��k����Ha����}�*���(����UM��7�GH�o�83
f��K�Z����)�Fm�F�lP�V5�[�I=B��Q��(j�A�Zղ���<"�v�������ͷa��Ң��x'b���|{3��f���n/iZތ�(����̠m_[���ö�#�B'��va�-���6i�n��o�_~����ܼ9� ��x�]ǯ�կs���/�jX��=B
�;ƿ��]��Ϸen٭��ǕM"��/'H�y�p7�)����Z�4�}�&HuWחO�(�Vo�+�����ֹ�A
���-��-��o�h��5�؉g�gkE˰�-�Q�&F3�g���ZѶk�)���'N�[����Z�4�[�f)�q�O.�el�M�a�k���8����D[=nn�)�n*����xY�������I��f�B�į�u'9�jw�5�\���?�wvwW�w�vwy�p�����.�ʺa��Ǎ����y���QS f#Ɵ5g9�I��s��.�%��/��Rh=��w�j�%^������h���F���������!�(Z.�im)��1{Z��	���v�v���qZ�<����j���Ϸ��)������_�������`ho����v�n�;� �h9(��դ�&�O?��h�����e��r{';�����ך��l�4�S��T�;*X�������x�����g�)�����m�wF"%���m}�E/ebK�)�/�<�� Z���`S��V�'�aS�!�X��1���S0=3�l]ɔ���:�֋s�����:���O�2��d�.Lq=��F�
��p�}*��+ڀ'.�ee%P2E��g�2��0P�Ҭ%-/����������M}#�n������k�)·2�R�l�q���ik}�EN�u&g�2$����/��H�G��I=���f}��2/����
�2���ׁ�|�K4��S���<R��'�ա�O����RyӉ�o5v3�3
f���>�bgn��9���Tv�a��%c�Ρr�����(�g��Mw��9� e|t�.�8O��ĮO��ei_j�aA�emp5Ϝe�Y��$�a�e�7��s)��k�z��0DRk-lG#�<~��38�"%�ܑ֖�8��pE�lei͞,���R?�I.�-��g�� o��p3R#�H�5R�z�de3+�>�D����r	�uC��?$Logn��V�FDt�H�� �5Z�e��=J�K��Qڝ��y����w�CorF�����	�leU0re�Bj�rx�� _FCF#�=Z{�9�����<0T�I���hv^Z6���چ��`>���q!E�qJr��ЗJԓ��l2Zr/%9%�B��q��&n����I� E�K!k�}��UrN��J����@KV�m��Y�YV�M��׫��Hu�7C26��1{�8C��ωm��C}���S�K�^GO5�Pd�*�����Pkp�����i܄9�Ⱦ9Vr`+��S��9��u��p�S�'j���Y쭀�iT�9��u�wV{��X/1ho���:-�� �:w��⠖��Ii�;i�e����r���K�,~jZ��C�����i�4},E�͑�}5�EBi�{<g]����-�9�
?*Rg���!Iz+T�XK-�Z�<�g9�����cI�U�kJ�؈=��e+�*�J�s�TS_�	�mI<�,ӗ��{��e��4L�q�OZj�4~/�-n-HQ83n�}-��;��T�� �WM%:�cD�N�����K-H1�=b!%�����˄˄��v�X�?(Zg�7�a�#e�HaǇ(ǚ`u��u��>�EY4H}��'�~u��1��7���y�lӂ��u$��a�0�%8Ȍ����C�C!�9J�&9�sK�j�~?��kn���K���F��*bp�x��l	��Z��#l=�����(���_�Z�b�>�)ezh���0�"͊Y곑*�(+�̦rr�KYKbK�3�~K�/����Rl�4�:`/z,:��w��^��T�E��O-��O��5Ԕi�u)v/��2���ј)k���A(�ꆤ����Q4]�޶i�|ghR(����"J0����C��V����ʭ*�9FV����=|jA���L��<��I�")N4u���*5mJ
0BV �"�2�HHT��t��5�Hw�����������߷�M1r��$LNj!���>�M`�)������1:��ߺ�3HY�~5r��6��yVz�Y�%��]�d�)�[[kp��N��8Ю�#�N�WN�H{���� e�e�:ٶ �+�fٹZ�%QCߐ�[ޏ��Z+Y|C�5�>j.�\�CKX<�t�FN]�O�0��=�vӗ��GZ����[1B���1ͣ�������A����h�!���c��*M��R�'tn�|��4s�B;��wt2,!e�e�q����Sr�rYGFk�Wm�U�i�2�;�?�����ܽ����������4�!"�S�,�-g_h��A�x=?�k�)3��HKHYA�X�a���'���D�W�TbXw��XX��gYY
D��t�_B�>����-���1�<m٧��{-�ͤ<��VL%�$�x�}�W2�ZՖ�u �����lxCl���ꥡ�a@z�$G)2��5�ņ������p4n�RNw�o#��p�HY�C�=aNS�&k�����a�b�#�sz��~����pax���ZB�{����$�      �   >  x�͑�N�@E��W���:���� ��"H����8���(�@KIC�>i��?�Q� ���)��$����G{��szv٥��
�]�J�P��ʱ|ϸ@ ���Ǖ�g�=�J	.�Ph����:-�7.w)W���z���0�c�J7��)㱐Vp��{��d�"���fS���D�(V��/Q�6$��?^iY�I�]Ӭپdt��DKf+6Y���X�ʝҁW�^j�2�+�Ô�i�ڢϔ�_�#H!��zD�a�x�a^���I5���|5m6��(\���&&ۓ��O}R"Q&�
F�M�'3���      �   �   x��οNA�z�)�'�����m+�J:
�fm4��7�3$jk�\�{�p0JLl�+���O%kHmɓj�|��ݘf}�������ҿ$�]�H���^�uZ�4����OJ�Z�"hc]�[	���&�Cf�ӳ��cȠm�^8�Fh�*�`���CU��P�H�T�=/��p8;����㧼�s��c��*/���:�Vx����� �I^�      �     x���Mr�0 �}����#��Yz�s< i��R��[ #%�me�&��Tߡ�%8Ϝ���_j?����*[�`US~J��Ħ5�l��[�-�	xb:`���v"Ѱ��贪+뱈0�{F=S9��|�g?���;�Yk��E�)u�V���!
�?E��g��t�8K:��1���Oj3������ٮ��P��g��߶�r��E= ���s��9
7Վ{~���)��\�\C�I�iX�����}U?�~��-'ۼv�v���K�yn��<����&U��GhG��M���s��u(�7�S���˶�B�H��&9}�u�P^V�����j�#���ֿqR��(�IN��g�A����ȶ�-�<?#��a&Ʒ�,8/b�r�M�W�9Z������/���e���*��G�^��?���X�޻��{�}�sߊ�;�[Ъ���,�9���^+�r6η�*�qlo$A͇{1'�3�暞|�W��/I���<�h���A������g\ܳ��������sn��E�m}��7��Gyw�K�yn��]�i~������}`q'x�
����˛��9K��L¹���Y�)_ؓzӾ�o(�#�|<��{��%�>����po�A_�;�z����'�u뚡�N����a������!�+|D���){H�j��!������>���~@��W�=ɪ���nV�pA���y�z��N񂜷��)O���h޿��!ؔ.���p�ի�FզiA�U��5�6�c�����"�      �   h  x�}�A�$!��}_rڀ)�2�ǚf5K����J(�'MH/n_���������pY�	A{�"H*���=��	A]�+`ף�F��j��fm����{�J.O�B�����_�`x(���\�s��(�:vO�Q�Z.�)D�\�7�D��$/D©�f�Pͭ�(�.ߛW��p��Hߪ}~�+���Ht�#�3?�?��7?���(2�p��H�4��G��,ķKbk�ˏS�HlڡyE�>ںF�S��]��|ƾ%'DrM���/"�k���\b��54;�"ŴC��\�{��?x�u�����o��Hu�r��kR�r��6���I��ڂ-u�i�ν#��D�+�!�      �   /  x�}ѿK1�����.	��\o��At�[1g��I�BG'W�ɭ�@z����Oz-�(�t{��O��`�ܮ?s��`tJ(�ĀIG�M�0�	�Ҝ�O"*�池���3���PP�1C�Q�=8i�{n�����V9��b������;�E7��1<�_{(DK����E��|���J4��hg�y�tެ0>��_ ����9�<NDk8m�7�yi�W����1��Y�u���G�������Z�(D8��rVepwPLn~�>m�2���fm���f7�F�L��N!=Nd�	![&��      �   �   x�u�K�!�p��O4�G=K��c��ڲ�u��B(��PVyq���0ϦS�_���(�����T�a4`�T\h�:}!�,j�d%�R�7��E������0Z�n�����X�.Ӟ$\7�&��y*9mPD�La0>�1���*�!�u*f��6��z���:������U�a6k7G��l������t0?��5�1��0$ί�%LU/��߄����      �   �  x��Z�r�6}��"�ImM ހ��T�x���f6�xR�گ_H�j�Lv��L���@����Ͽ?��k,ϟ��XB � �=Y�ӗ�N4Mi��"e�-U��LuY��<�R����{��zr������?A����3g�'���Gv��_Z~�J�S�ِ��Xi�/��BN{,�oп������3d@���'�2K8}����N�q�� UI�#q@c��:�2�k�l)��x�p&wb�9^7)G{?G�p���6_��#o�����h� � ���1 Ԉ9&DŁN���L��F���D&}
t �k�A�̵�����"�H��Tb`�������,��
,�ў�v�5<���9N�`�sD[-TK��M5ͅ��+��罍ty��[�J��w����]�#���.^}���M�ə8�Ew����y}J���p���jD�H�3��yܡ�izd�q���P
�f�ְ��Y��uNef{E@�1/���GMO���@�^Z�Ѩ�����������r�nR�]�7����9�f���-.p:�>������p��ױ�Q������kܟ��X���(ܯk��L��������2���=���=�֖�å�a�p^�·�vhѲ�:^�^���6�ء-#~>��U��⾧,6M
_���0|;�sk�t������ܵ6l�����p���';����a�:��(�4��>|�����5���І�kܿ���݇��y�Cp-I܇��ރH�7;S��%T��v�^lҟ�3M�����6Q]�ϒ(���w�v���ֹ����gcO�{�6Mʏ��f�#S�(K]T5G)����,3�[~�)���a��=y�RN�ӗ �%���q�;.����u�(*l��j�eZ�:�5r�bZ�|�t}`Zɝ,��_��6�=��#���Ԣ��l�zq��֗.����I?mܹal�s�[�&��?��@1�ׁ�5w��m�*�>J	��A��k�v�����hv���rc}t[�s�u_��ܒ�}3oZ������6C~�q���;R"��������O��<�L�Zn���/2C�q��ޠ�>�Û�����-?�W�ʹin[[�o��ӎ	�f�/1���4�[��+ �����N��.�E"�o���E��ڤ�Y�g�`�����K�qN)F#*���w�.Z���;�A����	�mR�<L�)R.٪JN��1{��p��y�v����6%�g��K`���Y�D������:�-�����1�ܚ��Y+��K* �T]|W���ba�G�LtR���u�R���_
XAs�B3&�QD���e��;��m�~������M�3�g�C�!@���\�d�U�u����
�j2�z��ߺ:uKo��C^�����@�vҮ��}j�Ma��@��'�L�֟rr��X�Y;7x�[�{ݤJ�:?����Ix��n��5D���I}W#[�?�򧻔?^)���S��C�@^��ޅ'��e`�b,�Ѳj����:l��G����z%z�z�T����k3���f�[D|6r�;26-*��u��u�z��vX�۱�\3����rf_	�Pm�s�@��������0�Ù����{>��i(�FC3��
������t�w��~HLR�߇���y�K��x�wl�m����8d"�"���UL���{:W�b8�*1�ʩ�D�o��|����퍘��1�.:_��S���oڄ���ɿ~�6e�f(b�%	\Uo�$�`��<?�f�4wO�]�wdݦı$�cF#%V6��,�>�њ�N� 0|��Н�@����hN���E��%��Ht�#C�ȐfJ�/�u��n�:�c߷�f�C9-?���~,C��,��e���m֗!0�e���Ь���c79Ȑ���-}��ɴ�}x��ێ�CSg�һc�Ȧ���>=>>����      �   O  x��Z]o�}�~�<�\zfv>iÀ*��H�!�I�~Q\K\��R��7(��I�hZ��H۠(Z�@��"�<���`I���\rI�r�hki͝�Μs�w������w���"Bݚ�s��^r�d���F�f��ֿܸY���Ԉ�>����7޼U�6���pp-�(%S�RL5Vn�h݂��Bb0�̤	R��֍��m��@z�j��d�3EC)d$ں�E�v[]o�F'�՚�	���ڝ�:07J,��Q��5]ݐ����I�a�d��qC(eLz�מ<>��.��x{m��@[�3Nk��u�ޣ���<'�m��F�޼1HO`�nnv�gH�q<�|�\O�A���k��t|�E����3X��x�E^pK��j�r?)��������zr؜jo4�����N<>�*EGp����5MA|\��^��wx�Fׇf:�B���v;7���C���	�Ϟ(Y�����i����vąI��b
�:2ƣ��#�|	�x�Q8>�`9}�����4�b�Pzb��䰎���J����;(��Pr���ƣ�Ũ;>�g�r���=����?���G��b=;?X��L���ݻ���n��d��z/[x%�e��՟p�M�w#D�5�s�Q�s�m�>���B�0f�0f\�Mߖ�$�"兞O���JG�i&q[�(,q	(�	��N���$M
|���
2�&�&�1��kw�����7o��G�?���Z{8�'��48a�8�!�����ye�'�០k��4��yC T��P������n;��iR7_��cd�����b�m��}��:��x<� ���j�L�Ȝ%J���PjƷ�?k���� Eo p�}a!/&��M��e}�K(1�0�їy�ɐD$�1�0E���𴯈1����>q��.o���q�@�"ty�!M����Ф*Έ&'���3�v��6�:o������t�3>���آ
�
D�Ћ�8�"NG�ㅙ%�paة>�Я;�:���']��O�::��A�y�> �{2�ƅTZ��a1H\fU ��L�l}YlJ�!��&
�"�|H���%���]�������p�+)0<�ml:~/���P�Gv)�^�����]����,�e5��	�ZS�<��P�u�?���������.Q�*Xj@�E(i?��X�T�ʰ�0��殌��PD*���)t��Ei���@��cm&|�܀
��6����*se.qWp��Q�He�´�mds�X��-3߀+���*Ӄ�b`�Q䄶-7}o�`�	C�2$�lژ�'Dfp# �<��m�B1�L@vZ��=�}xr��AUb8d�i-A�B�,��u�֑���i,-s�"���)(�E�o���!W�CB����b�A13&�F��ǣ_�$�~	����kh���_!��A�Қ1ЄA�A�C��wl{a�t��]^N�L��;�v|�E��6��i�C�;�A�w�.ϫ ���c�M8<��͈��/��ؕ�P�� �g:Ã+*��|݉���<7�"_w[-�%H��g^��|��3�C�Q1��8ǒ3���GH�n���>�d"|������}���y1�1$D���wz񬞲Ƣ���Ű�ox��c}���ҩ��N��,ʖg�LI��Y]�/����2%&y�&3�)ɗ��[��Yby�=�HV�@/�01��e�D���m��i8�_Ψ�\�aX�2���J�U�b�sCa^�gM���kͥ���m�;@��-�5��ǐ����oh�s�e�2ZE�W��\T0[)�.�����,�=�`lOapXJX33<<<���D�v�ک��"A5%�ü��H�+&*KK�Y�!�ۺ��*��i�p��m
9 �	�)- K�s<�&Ha���	�Q��OM|�-��@`��ք�-��b�,<�,n|�ly�oW��8wV�-!<C��*<�U�i 3��9hM�)k}��<2%���1�zf*���R�g����|Iй�_R���n���LZ��2�R�(C��+
b~E)Xdb��-D<�R�R��&�"�sd�X�<T����OQ7��<��.׵A���N2t�^�@�c���Tv_i���́}���Co��KT��b�
�o�2.�M���Z�E��@e�RYU�^�w�1���;�j��4��+#a-�F�T,�XE�˶O����R�(`�|�s�Z$îl0�*x��#I���j���?�C?:�c�����~b�Kk�s
���a�ks�w{BR]�1�,	�UT��v0C�\������$�sO���1u�3tW�s�E]_�m�i酐�S�(rL{sE�Ȝ���0IV���	�(S��ϚL��k��۽�o� /�d��.�����0Nm��$��!����|N˰H��I�f���hzȏz�NȿC��M���V�]y�g�����$ǒ��X��g��@)sb���"-�Ϙ�I�A7;��K�K� Q�4�br�`Z#�vp�������m��qm��~M8����y���}'������0,�-�\�&��Nq�yJ ��zV����{�kC\��Yا�üd��l8��m�Ad���{�5��ɞ!����֣��6C&�v/K�p�����S�E���y�4���(�㞑�y��f8S���;FxK���LE�(��� �@�@��
��5�p�d�ᆴ�B�M${MBM^� 2�a��%8Bs|���6:�XI:}˜�I��ެC��e@g�\�N���Y�=���o��z���o��K�sZ�J L�Xx��V�Y!�J�tz�k�e��K��#U{Zp7{?�8X��{v��MR��9�M����L����ÈjE<�朡v#���.�MJ�/��9�\��X[[�/�@�      �   �   x�����0E��W�n��/h���	lML���PJJY㿋#a����̝� :�w���-���7)D�yטIvޕC��K�����Ni�������f@޸j���ִ3˚�֓�Mu��F���H��Bq��Ă�=�%��ɵa䏷,�~�����S��t�(A2ݘ���/6���"Y����
��r��S�p/      �   �  x�}��n�@���Sx�]�0?�x�$����"Uv�M ��誛J]T]Tj�R�e�*����&�ЄF�|��X���96��� �u�j�=����t0:ʃ�0G���z7��l�g��n�8�<�����3x~X��Q�� �����E,��@P��y� ��;$o%�������R骑\
<k��%eF��J�T�UE�G�5������խC����mV�/mu.���"���Zt<ao��2D��� H��&��0\��}`ث�'���dv�t탿�}bR-��C��z�λ�d�lӨ�5����=i�����S��ϕ�I0//@TT�)�&	���?ꦓ���=;텬PI�����>�%/$�Cc��漜y4��X�\��3�#�`���j����[aݮ����dJm�^n�ujz0w��gj�&��z�,�X������XZ0���d�$�}�[-�'���/�-�SdyI��grIS�_K�tӵ�t�5��,���6���L~��r\��Z�&R�>����3ÉM�pz-G}= G]l�7�r-^�k�=M�����( �~`�A��iS��c�X�F'�|Ñ��5�6���Nث�^8�V��ٰsR%���iز�NE1翀b��`{w� ���S�w2�Yz���ǲ�uq+>m�H�(M=�èZ��z�4���v݌F�P+��sC��U�&}���Ȃ@^Yb��Rn�@Q���ZZ��
f2��?ym     