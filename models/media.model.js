const sequelizePaginate = require('sequelize-paginate')
module.exports = (sequelize, Sequelize) => {
    const Media = sequelize.define("media", {
        name: {
            type: Sequelize.STRING
        },
        url: {
            type: Sequelize.STRING
        }
    });
    sequelizePaginate.paginate(Media)
    return Media;
};