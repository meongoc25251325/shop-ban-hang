module.exports = (sequelize, Sequelize) => {
    const OrderItem = sequelize.define("order-items", {
        order_id: {
            type: Sequelize.BIGINT
        },
        product_id: {
            type: Sequelize.BIGINT
        },
        product_name:{
            type: Sequelize.STRING
        },
        product_slug:{
            type: Sequelize.STRING
        },
        product_price:{
            type: Sequelize.BIGINT
        },
        product_image:{
            type: Sequelize.TEXT
        },
        product_attribute:{
            type: Sequelize.JSON
        },
        quantity:{
            type: Sequelize.BIGINT
        },
    });
    return OrderItem;
};