const sequelizePaginate = require('sequelize-paginate')
module.exports = (sequelize, Sequelize) => {
    const AttributeItem = sequelize.define("attribute_items", {
        attribute_id: {
            type: Sequelize.BIGINT
        },
        name: {
            type: Sequelize.STRING
        },
        background: {
            type: Sequelize.STRING
        }
    });
    sequelizePaginate.paginate(AttributeItem)
    return AttributeItem;
};