module.exports = (sequelize, Sequelize) => {
    const ProductImages = sequelize.define("product_images", {
        product_id: {
            type: Sequelize.BIGINT
        },
        image: {
            type: Sequelize.STRING
        }
    });
    return ProductImages;
};