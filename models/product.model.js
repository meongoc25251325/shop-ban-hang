const sequelizePaginate = require('sequelize-paginate')
module.exports = (sequelize, Sequelize) => {
    const Product = sequelize.define("products", {
        name: {
            type: Sequelize.STRING
        },
        slug: {
            type: Sequelize.STRING
        },
        sku:{
            type: Sequelize.STRING
        },
        description:{
            type: Sequelize.TEXT
        },
        price:{
            type: Sequelize.BIGINT
        },
        sale_price:{
            type: Sequelize.BIGINT
        },
        category_id:{
            type: Sequelize.BIGINT
        },
        label:{
            type: Sequelize.STRING
        },
        image:{
            type: Sequelize.TEXT
        },
        status:{
            type: Sequelize.STRING
        },
        stock:{
            type: Sequelize.BIGINT
        },
    });
    sequelizePaginate.paginate(Product)
    return Product;
};