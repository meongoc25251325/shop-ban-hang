const dbConfig = require("../configs/db");
const Sequelize = require("sequelize");
const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
    host: dbConfig.HOST,
    dialect: dbConfig.dialect,
    operatorsAliases: false,
    pool: {
        max: dbConfig.pool.max,
        min: dbConfig.pool.min,
        acquire: dbConfig.pool.acquire,
        idle: dbConfig.pool.idle
    }
});
const db = {};
db.Sequelize = Sequelize;
db.sequelize = sequelize;
db.user = require("./user.model")(sequelize, Sequelize);
db.Role = require("./roles.model")(sequelize, Sequelize);
db.media = require("./media.model")(sequelize, Sequelize);
db.Product = require("./product.model")(sequelize, Sequelize);
db.ProductCategory = require("./product-category.model")(sequelize, Sequelize);
db.ProductCategoryMap = require("./product-categories-map.model")(sequelize, Sequelize);
db.ProductImages = require("./product-images.model")(sequelize, Sequelize);
db.ProductAttribute = require("./product-attributes.model")(sequelize, Sequelize);
db.ProductAttributeItem = require("./product-attribute-items.model")(sequelize, Sequelize);
db.OrderItem = require("./order-item.model")(sequelize, Sequelize);
db.Order = require("./order.model")(sequelize, Sequelize);
db.Attributes = require("./attributes.model")(sequelize, Sequelize);
db.AttributeItems = require("./attribute-items.model")(sequelize, Sequelize);
db.Blog = require("./blog.model")(sequelize, Sequelize);

db.Product.hasMany(db.ProductCategoryMap, { as:"categories",sourceKey: 'id', foreignKey: 'product_id' });
db.Product.hasMany(db.ProductImages, { as:"gallery",sourceKey: 'id', foreignKey: 'product_id' });
db.Product.hasMany(db.ProductAttribute, { as:"attributes",sourceKey: 'id', foreignKey: 'product_id' });

db.ProductAttribute.hasOne(db.Attributes, { as:"attribute",sourceKey: 'attribute_id', foreignKey: 'id' });
db.ProductAttribute.hasMany(db.AttributeItems, { as:"items",sourceKey: 'attribute_id', foreignKey: 'attribute_id' });
//db.ProductAttribute.hasMany(db.ProductAttributeItem, { as:"values",sourceKey: 'id', foreignKey: 'product_attibute_id' });

db.ProductAttribute.belongsToMany(db.AttributeItems, { as:"values",sourceKey: 'id', foreignKey: 'product_attribute_id',through: db.ProductAttributeItem });
db.AttributeItems.belongsToMany(db.ProductAttribute, { as:"product_attribute",sourceKey: 'id', foreignKey: 'attribute_item_id', through: db.ProductAttributeItem });

db.Attributes.hasMany(db.AttributeItems, { as:"items",sourceKey: 'id', foreignKey: 'attribute_id' });

db.ProductCategory.hasMany(db.ProductCategoryMap, { as:"products",sourceKey: 'id', foreignKey: 'product_category_id' });

db.Order.hasMany(db.OrderItem, { as:"items",sourceKey: 'id', foreignKey: 'order_id' });
db.Order.hasOne(db.user, { as:"user",sourceKey: 'user_id', foreignKey: 'id' });

db.user.hasOne(db.Role, { as:"role",sourceKey: 'role_id', foreignKey: 'id' });
//db.user.hasMany(db.Order, { as:"orders",sourceKey: 'id', foreignKey: 'user_id' });

module.exports = db;