const moment = require('moment');
const sequelizePaginate = require('sequelize-paginate')
module.exports = (sequelize, Sequelize) => {
    const Blog = sequelize.define("blogs", {
        title: {
            type: Sequelize.STRING
        },
        slug: {
            type: Sequelize.STRING
        },
        content:{
            type: Sequelize.TEXT
        },
        description:{
            type: Sequelize.TEXT
        },
        image:{
            type: Sequelize.STRING
        },
        status:{
            type: Sequelize.STRING
        },
        createdAt: {
            type: Sequelize.DATE,
            get() {
                return moment(this.getDataValue('createdAt')).lang("vi").format('dddd, DD [tháng] MM [năm] YYYY');
            }
        },
    });
    sequelizePaginate.paginate(Blog)
    return Blog;
};