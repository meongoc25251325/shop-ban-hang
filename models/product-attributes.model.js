module.exports = (sequelize, Sequelize) => {
    const ProductAttribute = sequelize.define("product_attributes", {
        product_id: {
            type: Sequelize.BIGINT
        },
        attribute_id: {
            type: Sequelize.BIGINT
        }
    });
    return ProductAttribute;
};