const moment = require('moment');
const sequelizePaginate = require('sequelize-paginate')
module.exports = (sequelize, Sequelize) => {
    const User = sequelize.define("users", {
        role_id: {
            type: Sequelize.BIGINT
        },
        first_name: {
            type: Sequelize.STRING
        },
        last_name: {
            type: Sequelize.STRING
        },
        full_name: {
            type: Sequelize.STRING
        },
        username: {
            type: Sequelize.STRING
        },
        password: {
            type: Sequelize.STRING
        },
        phone_number: {
            type: Sequelize.STRING
        },
        address: {
            type: Sequelize.TEXT
        },
        is_admin: {
            type: Sequelize.BOOLEAN
        },
        status: {
            type: Sequelize.STRING
        },
        createdAt: {
            type: Sequelize.DATE,
            get() {
                return moment(this.getDataValue('createdAt')).lang("vi").format('dddd, DD [tháng] MM [năm] YYYY HH:mm');
            }
        },
    });
    sequelizePaginate.paginate(User)


    return User;
};