module.exports = (sequelize, Sequelize) => {
    const ProductAttributeItem = sequelize.define("product_attribute_items", {
        product_attribute_id: {
            type: Sequelize.BIGINT
        },
        attribute_item_id: {
            type: Sequelize.BIGINT
        }
    });
    return ProductAttributeItem;
};