const moment = require('moment');
const sequelizePaginate = require('sequelize-paginate')
module.exports = (sequelize, Sequelize) => {
    const Order = sequelize.define("orders", {
        order_number: {
            type: Sequelize.STRING
        },
        user_id: {
            type: Sequelize.BIGINT
        },
        item_price:{
            type: Sequelize.BIGINT
        },
        total_price:{
            type: Sequelize.BIGINT
        },
        payment_method:{
            type: Sequelize.STRING
        },
        ship_method:{
            type: Sequelize.STRING
        },
        ship_price:{
            type: Sequelize.BIGINT
        },
        first_name:{
            type: Sequelize.STRING
        },
        last_name:{
            type: Sequelize.STRING
        },
        address:{
            type: Sequelize.STRING
        },
        phone_number:{
            type: Sequelize.STRING
        },
        status:{
            type: Sequelize.STRING
        },
        createdAt: {
            type: Sequelize.DATE,
            get() {
                return moment(this.getDataValue('createdAt')).lang("vi").format('HH:mm dddd, DD [tháng] MM [năm] YYYY');
            }
        },
    });
    sequelizePaginate.paginate(Order)
    return Order;
};