module.exports = (sequelize, Sequelize) => {
    const ProductCategoryMap = sequelize.define("product_categories_map", {
        product_id: {
            type: Sequelize.BIGINT
        },
        product_category_id: {
            type: Sequelize.BIGINT
        }
    });
    return ProductCategoryMap;
};