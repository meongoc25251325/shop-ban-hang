const sequelizePaginate = require('sequelize-paginate')
module.exports = (sequelize, Sequelize) => {
    const Attribute = sequelize.define("attributes", {
        name: {
            type: Sequelize.STRING
        }
    });
    sequelizePaginate.paginate(Attribute)
    return Attribute;
};