const sequelizePaginate = require('sequelize-paginate')
module.exports = (sequelize, Sequelize) => {
    const Role = sequelize.define("roles", {
        name: {
            type: Sequelize.STRING
        },
        actions:{
            type: Sequelize.JSON
        }
    });
    sequelizePaginate.paginate(Role)
    return Role;
};