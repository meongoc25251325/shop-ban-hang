### Hướng dẫn sử dụng
#### Hướng dẫn cài đặt thư viện và chạy dự án
- Tải dự án về máy: `git clone https://gitlab.com/meongoc25251325/shop-ban-hang.git`
- Chạy lệnh `cd shop-ban-hang`
- Cài đặt thư viện cho dự án `npm install`
- Khởi động dự án `node bin\www`
- Truy cập [http://localhost:3000/](http://localhost:3000/) để xem front-end
- Truy cập [http://localhost:3000/admin](http://localhost:3000/admin) để xem backend
- Tài khoản demo: `admin/123456`

#### Hướng dẫn cài đặt database
- Mở postgresql (Sử dụng pgAdmin)
- Tạo database với tên `shop`
- Restore database với file `database.sql` trong `shop-ban-hang/database`
 
