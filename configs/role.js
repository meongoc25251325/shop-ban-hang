module.exports = {
    role_mdules:[
        {key : "user" , name : "Quản lý tài khoản"},
        {key : "role" , name : "Quản lý phân quyền"},
        {key : "product" , name : "Quản lý sản phẩm"},
        {key : "product_category" , name : "Quản lý danh mục sản phẩm"},
        {key : "order" , name : "Quản lý đơn hàng"},
        {key : "attribute" , name : "Quản lý thuộc tính"},
        {key : "blog" , name : "Quản lý bài viết"},
        {key : "comment" , name : "Quản lý bình luận"},
        {key : "media" , name : "Quản lý hình ảnh"},
        {key : "setting" , name : "Cài đặt"},
    ]
};