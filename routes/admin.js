const express = require('express');
const router = express.Router();
const path = require('path')
const multer = require('multer');
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'public/uploads')
    },
    filename: function (req, file, cb) {
        let fileName = file.originalname.split('.');
        fileName.pop();
        fileName = fileName.join(".");
        cb(null, fileName + '-' + Date.now() + path.extname(file.originalname))
    }
})
const upload = multer({
    storage: storage,
    fileFilter: function (req, file, callback) {
        let ext = path.extname(file.originalname);
        if(ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg' && ext !== '.webp') {
            return callback(new Error('Only images are allowed'))
        }
        callback(null, true)
    },
})


//Use Controllers
const userController = require("../controllers/admin/user.controller");
const mediaController = require("../controllers/admin/media.controller");
const productController = require("../controllers/admin/product.controller");
const productCategoryController = require("../controllers/admin/product-category.controller");
const AttributeController = require("../controllers/admin/attribute.controller");
const blogController = require("../controllers/admin/blog.controller");
const orderController = require("../controllers/admin/order.controller");
const roleController = require("../controllers/admin/role.controller");


//Admin home page
router.get('/', function(req, res, next) {
    //Setup breadcrumbs
    req.breadcrumbs('Dashboard');
    res.render('admin/dashboard', { title: 'Trang quản trị', breadcrumbs: req.breadcrumbs() });
});

//Admin user management
router.get('/users', userController.index);
router.get('/users/delete', userController.delete);
router.get('/users/:id(\\d+)', userController.detail);
router.post('/users/:id(\\d+)', userController.save);

//Role management
router.get('/roles',roleController.index);
router.get('/roles/delete', roleController.delete);
router.get('/roles/:id(\\d+)', roleController.detail);
router.post('/roles/:id(\\d+)', roleController.save);

router.get('/logout', async (req, res, next) => {
    req.session.user = null;
    res.redirect('/login');
});

//Product category
router.get('/product-category', productCategoryController.index);
router.get('/product-category/delete', productCategoryController.delete);
router.get('/product-category/:id(\\d+)', productCategoryController.detail);
router.post('/product-category/:id(\\d+)', productCategoryController.save);

//Product
router.get('/products',productController.index);
router.get('/products/delete',productController.delete);
router.get('/products/:id(\\d+)', productController.detail);
router.post('/products/:id(\\d+)', productController.save);

//Order
router.get('/orders',orderController.index);
router.get('/orders/delete',orderController.delete);
router.get('/orders/:id(\\d+)', orderController.detail);
router.post('/orders/:id(\\d+)', orderController.save);

//Attributes
router.get('/attributes',AttributeController.index);
router.get('/attributes/delete', AttributeController.delete);
router.get('/attributes/:id(\\d+)', AttributeController.detail);
router.post('/attributes/:id(\\d+)', AttributeController.save);
router.post('/attributes/:id(\\d+)/add-value', AttributeController.addValue);
router.post('/attributes/:id(\\d+)/delete-value', AttributeController.deleteValue);


//Blog
router.get('/blogs',blogController.index);
router.get('/blogs/delete', blogController.delete);
router.get('/blogs/:id(\\d+)', blogController.detail);
router.post('/blogs/:id(\\d+)', blogController.save);

//Admin media manager
router.get('/media', mediaController.index);
router.get('/media/json', mediaController.json);
router.post('/media/delete/:id(\\d+)', mediaController.delete);
router.post('/media/upload',upload.single('file'), mediaController.upload);

//Setting
router.get('/settings',function(req, res, next){
    req.breadcrumbs('Cài đặt');
    res.render('admin/coming-soon', { title: 'Đang phát triển', breadcrumbs: req.breadcrumbs() });
});

//Comment

router.get('/comments',function(req, res, next){
    req.breadcrumbs('Bình luận');
    res.render('admin/coming-soon', { title: 'Đang phát triển', breadcrumbs: req.breadcrumbs() });
});




module.exports = router;
