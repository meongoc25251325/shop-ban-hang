const express = require('express');
const router = express.Router();

const userController = require("../controllers/frontend/user.controller");
const pageController = require("../controllers/frontend/pages.controller");
const productController = require("../controllers/frontend/product.controller");
const blogController = require("../controllers/frontend/blog.controller");

/* GET home page. */
router.get('/', pageController.home);
router.get('/gioi-thieu',pageController.about);
router.get('/lien-he',pageController.contact);
router.get('/404', pageController.page404);

//Blog page
router.get('/tin-tuc',blogController.blogs);
router.get('/tin-tuc/:slug',blogController.blogDetail);


//User page
router.get('/dang-nhap',pageController.login);
router.get('/dang-ky',pageController.register);
router.post('/login',userController.loginAdmin);
router.post('/dang-nhap',userController.loginFrontEnd);
router.get('/dang-xuat', userController.logout);
router.post('/dang-ky',userController.register);
router.get('/tai-khoan',userController.profile);
router.post('/tai-khoan',userController.updateProfile);

//Cart & Order page
router.get('/gio-hang',productController.cartPage);
router.get('/thanh-toan',productController.checkoutPage);
router.post("/thanh-toan",productController.order)
router.get("/don-dat-hang/:order_number",productController.orderdetail)

//Product & Product category page
router.get('/san-pham',productController.products);
router.get('/san-pham/:slug',productController.productDetail);
router.get('/danh-muc/:slug',productController.productCategory);
router.get('/them-gio-hang',productController.addToCart);
router.get('/them-gio-hang-ajax',productController.addToCartAjax);
router.get('/xoa-gio-hang-ajax',productController.removeToCartAjax);
router.get('/cap-nhat-so-luong-gio-hang',productController.updateCart);
router.get('/test-gio-hang',(req, res, next)=>{
    res.send(req.session.cart)
});

//Admin login page
router.get('/login', function(req, res, next) {
    res.render('admin/login', { title: 'Đăng nhập quản trị'});
});


module.exports = router;
