(function($){
    //Tick all in checkbox

    function checkAll() {
        $('#checkallTable').click(function(e){
            var table= $(e.target).closest('table');
            $('td input.checkallTable',table).prop('checked',this.checked);
            if(table.find("input.checkallTable:checked").length > 0){
                $(".btn-delete-record-in-table").addClass("d-inline-block.").removeClass("d-none");
            }else{
                $(".btn-delete-record-in-table").addClass("d-none").removeClass("d-inline-block.");
            }
        });
    }

    function checkHasChecked() {
        $('.checkallTable').click(function(e){
            var table= $(e.target).closest('form');
            if(table.find("input.checkallTable:checked").length > 0){
                $(".btn-delete-record-in-table").addClass("d-inline-block.").removeClass("d-none");
            }else{
                $(".btn-delete-record-in-table").addClass("d-none").removeClass("d-inline-block.");
            }
        });
    }


    function init(){
        checkAll();
        checkHasChecked();
    }

    $(document).ready(function () {
        init();
    });
})(jQuery);
