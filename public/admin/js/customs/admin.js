(function($){

    function moneyFormat() {
        $('.money').mask('000,000,000,000,000', {reverse: true});
    }

    function addLineItem() {
        $("#addLineItem").find("form").submit(function(e){
            e.preventDefault();
            var name = $(this).find("#name").val();
            var background = $(this).find("#background").val();
            var url = $(this).attr("action");
            if(!name) return alert("Vui lòng nhập giá trị");
            $.ajax({
                url: url,
                type: "post",
                data: {
                    name : name,
                    background : background
                },
                success: function (response) {
                    $(".attribute-values").append('<div class="line-item"><span>'+response.name+'</span><button class="delete-line-item" type="button" data-id="'+response.id+'">Xóa</button></div>');
                    $("#addLineItem").modal("hide");
                },
            })
        })
    }

    function deleteLineItem() {
        $(document).on("click",".attribute-values .delete-line-item",function(e){
           e.preventDefault();
           var id = $(this).data("id");
           var that = $(this);
           if(id){
               if (confirm('Bạn có chắc chắn muốn xóa giá trị?')) {
                   $.ajax({
                       url: "/admin/attributes/" + id + "/delete-value",
                       type: "post",
                       success: function (response) {
                           that.closest(".line-item").remove();
                       },
                   })
               }
           }
        });

    }

    function productAddAtrribute() {
        $(".add-attribute #add-product-attribute").click(function (e) {
            e.preventDefault();
            var attribute_id = $(this).closest(".add-attribute").find("select#select-attribute").val();
            if(!attribute_id){
                return alert("Hãy chọn thuộc tính để thêm")
            }
            var template_id = "#attribute_template_" + attribute_id;
            var attribute_item_html = $(template_id).html();
            $(this).closest(".card-body").find(".list-attribute").append(attribute_item_html);
            $(this).closest(".add-attribute").find("select#select-attribute").val("");
            $(this).closest(".add-attribute").find("select#select-attribute").find("option[value="+attribute_id+"]").prop('disabled',true);
        })
    }
    
    function productDeleteAtrribute() {
        $(document).on("click",".product-detail .delete-product-attribute",function (e) {
            e.preventDefault();
            var attribute_id = $(this).data("id");
            $(this).closest(".card-body").find(".add-attribute select#select-attribute").find("option[value="+attribute_id+"]").prop('disabled',false);
            $(this).closest(".item").remove();
        })
    }

    function moneyFormatSpan() {
        $('span.price-format').mask('000.000.000.000.000', {reverse: true});
    }

    function init(){
        moneyFormat();
        addLineItem();
        deleteLineItem();
        productAddAtrribute();
        productDeleteAtrribute();
        moneyFormatSpan();
    }

    $(document).ready(function () {
        init();
    });
})(jQuery);
