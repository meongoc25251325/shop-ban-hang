(function($){
    function fileUpload(){
        $("div#fileUploadForm").dropzone({
            url: "/admin/media/upload",
            previewsContainer:"#listMedia ul",
            previewTemplate: document.querySelector('#preview-template-container').innerHTML,
            success: function (file, response) {
                file.previewElement.classList.add("dz-success")
                $(file.previewElement).attr("data-id",response.id);
                $(file.previewElement).attr("data-name",response.name);
                $(file.previewElement).attr("data-url",response.url);
            },
            error : function (file, response) {
                $(file.previewElement).remove();
            },
        });

    }

    function openDetailMedia() {
        $(document).on("click", "#listMedia .media-item", function(e) {
            e.preventDefault();
            var url = $(this).data("url");
            var name = $(this).data("name");
            var id = $(this).data("id");
            var detail = $("#mediaDetail");
            detail.find(".preview img").attr("src",url).attr("alt",name);
            detail.find(".info .url a").attr("href",url).html(url);
            detail.find(".info input#name").val(name);
            detail.find(".delete-image-url").attr("href","/admin/media/delete/" + id);
            detail.find("form").attr("action","/admin/media/edit/" + id);
            detail.find("form input[name=id]").val(id);
            detail.modal("show");
        });
    }

    function deleteImage() {
        $(".delete-image-url").click(function (e) {
            e.preventDefault();
            var url = $(this).attr("href");
            //call to delete image
            if (confirm('Bạn có chắc chắn muốn xóa hình ảnh?')) {
                $.ajax({
                    type: "POST",
                    url: url,
                    success: function(result){
                        var id =   $("#mediaDetail").find("input[name=id]").val();
                        var selector = '#listMedia .media-item[data-id="'+id+'"]';
                        $(selector).remove();
                        $("#mediaDetail").modal("hide");
                    }
                });
            } else {

            }
        })
    }

    function init(){
        fileUpload();
        openDetailMedia();
        deleteImage();

    }

    $(document).ready(function () {
        init();
    });
})(jQuery);
