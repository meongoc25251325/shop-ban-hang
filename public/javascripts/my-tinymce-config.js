editorGlobal = null;
imageSelect = null;
tinymce.init({
    selector: 'textarea.advanced-editor',
    remove_script_host: false,
    convert_urls: false,
    height: 500,
    menubar: false,
    plugins: [
        'advlist autolink lists link image charmap print preview anchor',
        'searchreplace visualblocks code fullscreen',
        'insertdatetime media table paste code help wordcount'
    ],
    toolbar: 'undo redo | formatselect | ' +
        'bold italic backcolor | link uploadmedia| alignleft aligncenter ' +
        'alignright alignjustify | bullist numlist outdent indent | ' +
        'removeformat | code | help',
    setup: function (editor) {
        editor.ui.registry.addButton('uploadmedia', {
            icon: 'edit-image',
            tooltip: 'Insert media',
            onAction: function (_) {
                $("#modal-editor-select-media").modal("show");
                editorGlobal = editor
            }
        });
    },
});
tinymce.init({
    selector: 'textarea.basic-editor',
    height: 500,
    menubar: false,
    plugins: [
        'advlist autolink lists link image charmap print preview anchor',
        'searchreplace visualblocks code fullscreen',
        'insertdatetime media table paste code help wordcount'
    ],
    toolbar: 'undo redo | formatselect | ' +
        'bold italic backcolor | alignleft aligncenter ' +
        'alignright alignjustify | bullist numlist outdent indent | ' +
        'code | help'
});

$(document).ready(function () {

    function loadMedia(){
        $.ajax({
            url: "/admin/media/json",
            type: "GET",
            success: function (response) {
                if(response.docs){
                    for (const item of response.docs) {
                        var template = "<li class='media-item' data-url='" + item.url + "'><div class='thumb-image'><img src='"+ item.url +"'/></div></li>"
                        $("#modal-editor-select-media").find(".list-media ul").append(template);
                    }
                }
            }
        })
    }

    function fileUpload() {
        $("#modal-editor-select-media").find("div#fileUploadForm").dropzone({
            url: "/admin/media/upload",
            previewsContainer: "#modal-editor-select-media #listMedia ul",
            previewTemplate: document.querySelector('#preview-template-container').innerHTML,
            success: function (file, response) {
                file.previewElement.classList.add("dz-success")
                $(file.previewElement).attr("data-id", response.id);
                $(file.previewElement).attr("data-name", response.name);
                $(file.previewElement).attr("data-url", response.url);
            },
            error: function (file, response) {
                $(file.previewElement).remove();
            },
        });

    }

    function selectImage(){
        $(document).on("click", "#modal-editor-select-media .media-item", function (e) {
            e.preventDefault();
            var url = $(this).data("url");
            $("#modal-editor-select-media .media-item").removeClass("selected");
            $(this).addClass("selected");
            imageSelect = url;
        });
    }

    function selectImageFinal(){
        $(".select-image-final").click(function(){
            console.log(imageSelect);
            if(imageSelect){
                editorGlobal.insertContent("<img src='"+imageSelect+"'/>");
            }
            $("#modal-editor-select-media").modal("hide");
        })
    }

    function hiddenModal(){
        $("#modal-editor-select-media").on('hidden.bs.modal',function(){
            imageSelect = null
        });
    }
    loadMedia();
    fileUpload();
    selectImage();
    selectImageFinal();
    hiddenModal();
})