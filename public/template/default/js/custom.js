(function($){
    //Slider
    function sliders() {
        $('.single-slider').slick({
            autoplay : true
        });
    }

    function moneyFormat() {
        $('span.price-format').mask('000.000.000.000.000', {reverse: true});
    }

    function sortingChanged() {
        $("select#orderby").change(function(e){
            $(this).closest("form").submit();
        });
    }

    function addToCart() {
        $(".addtocartajax").click(function (e) {
            e.preventDefault();
            var product_id = $(this).data("id")
            $.ajax({
                url: '/them-gio-hang-ajax',
                type:'get',
                data:{
                    product_id:product_id,
                    quant:1,
                    attributes:null,
                },
                success: function success(response) {
                    $('header .sinlge-bar.shopping').html(response);
                    $("#confirmCart").modal();
                },
            })
        })
    }
    function removeToCart() {
        $(document).on("click",".delete-cart-item",function (e) {
            e.preventDefault();
            var index = $(this).data("index");
            var that = $(this)
            $.ajax({
                url: '/xoa-gio-hang-ajax',
                type:'get',
                data:{
                    index:index,
                },
                success: function success(response) {
                    if(that.hasClass("reload")){
                        location.reload();
                    }else{
                        $('header .sinlge-bar.shopping').html(response);
                        that.closest(".cart-item").remove();
                    }
                },
            })
        })
    }

    function updateQuantityCart(){
        $(".cart-change-quantity").change(function (e){
            //$(this).closest(".shopping-cart.section").addClass("loading");
            var index = $(this).data("index");
            var quantity = parseInt($(this).val());
            var that = $(this)
            if(!quantity) return;
            $.ajax({
                url: '/cap-nhat-so-luong-gio-hang',
                type:'get',
                data:{
                    index:index,
                    quantity:quantity,
                },
                dataType:"json",
                success: function success(response) {
                    //that.closest(".shopping-cart.section").removeClass("loading");
                    $('header .sinlge-bar.shopping').html(response.miniCart);
                    $('#total-amount-all-items .price-format').html(response.totalCart);
                    $('#total-cart-amount .price-format').html(response.totalCart);
                    that.closest("tr.cart-item").find(".total-amount .price-format").html(response.rowPrice);
                    $( ".price-format").mask('000.000.000.000.000')
                    $( ".price-format").mask('000.000.000.000.000', {reverse: true})
                },
                error : function (error){
                    //that.closest(".shopping-cart.section").removeClass("loading");
                }
            })
        })
    }

    function init(){
        sliders();
        moneyFormat();
        sortingChanged();
        addToCart();
        removeToCart();
        updateQuantityCart();
    }

    $(document).ready(function () {
        init();
    });
})(jQuery);
